<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted' => ':attribute debe ser aceptado.',
    'active_url' => ':attribute no es una URL válida.',
    'after' => ':attribute debe ser una fecha posterior a :date.',
    'after_or_equal' => ':attribute debe ser una fecha posterior o igual a :date.',
    'alpha' => ':attribute sólo debe contener letras.',
    'alpha_dash' => ':attribute sólo debe contener letras, números y guiones.',
    'alpha_num' => ':attribute sólo debe contener letras y números.',
    'array' => ':attribute debe ser un conjunto.',
    'before' => ':attribute debe ser una fecha anterior a :date.',
    'before_or_equal' => ':attribute debe ser una fecha anterior o igual a :date.',
    'between' => [
        'numeric' => ':attribute tiene que estar entre :min - :max.',
        'file' => ':attribute debe pesar entre :min - :max kilobytes.',
        'string' => ':attribute tiene que tener entre :min - :max caracteres.',
        'array' => ':attribute tiene que tener entre :min - :max ítems.',
    ],
    'boolean' => 'El campo :attribute debe tener un valor verdadero o falso.',
    'confirmed' => 'La confirmación de :attribute no coincide.',
    'date' => ':attribute no es una fecha válida.',
    'date_format' => ':attribute no corresponde al formato :format.',
    'different' => ':attribute y :other deben ser diferentes.',
    'digits' => ':attribute debe tener :digits dígitos.',
    'digits_between' => ':attribute debe tener entre :min y :max dígitos.',
    'dimensions' => 'Las dimensiones de la imagen :attribute no son válidas.',
    'distinct' => 'El campo :attribute contiene un valor duplicado.',
    'email' => ':attribute no es un correo válido',
    'exists' => ':attribute es inválido.',
    'file' => 'El campo :attribute debe ser un archivo.',
    'filled' => 'El campo :attribute es obligatorio.',
    'image' => ':attribute debe ser una imagen.',
    'in' => ':attribute es inválido.',
    'in_array' => 'El campo :attribute no existe en :other.',
    'integer' => ':attribute debe ser un número entero.',
    'ip' => ':attribute debe ser una dirección IP válida.',
    'ipv4' => ':attribute debe ser un dirección IPv4 válida',
    'ipv6' => ':attribute debe ser un dirección IPv6 válida.',
    'json' => 'El campo :attribute debe tener una cadena JSON válida.',
    'max' => [
        'numeric' => ':attribute no debe ser mayor a :max.',
        'file' => ':attribute no debe ser mayor que :max kilobytes.',
        'string' => ':attribute no debe ser mayor que :max caracteres.',
        'array' => ':attribute no debe tener más de :max elementos.',
    ],
    'mimes' => ':attribute debe ser un archivo con formato: :values.',
    'mimetypes' => ':attribute debe ser un archivo con formato: :values.',
    'min' => [
        'numeric' => 'El tamaño de :attribute debe ser de al menos :min.',
        'file' => 'El tamaño de :attribute debe ser de al menos :min kilobytes.',
        'string' => ':attribute debe contener al menos :min caracteres.',
        'array' => ':attribute debe tener al menos :min elementos.',
    ],
    'not_in' => ':attribute es inválido.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => ':attribute debe ser numérico.',
    'present' => 'El campo :attribute debe estar presente.',
    'regex' => 'El formato de :attribute es inválido.',
    'required' => 'El campo :attribute es obligatorio.',
    'required_if' => 'El campo :attribute es obligatorio cuando :other es :value.',
    'required_unless' => 'El campo :attribute es obligatorio a menos que :other esté en :values.',
    'required_with' => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all' => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_without' => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de :values estén presentes.',
    'same' => ':attribute y :other deben coincidir.',
    'size' => [
        'numeric' => 'El tamaño de :attribute debe ser :size.',
        'file' => 'El tamaño de :attribute debe ser :size kilobytes.',
        'string' => ':attribute debe contener :size caracteres.',
        'array' => ':attribute debe contener :size elementos.',
    ],
    'string' => 'El campo :attribute debe ser una cadena de caracteres.',
    'timezone' => 'El :attribute debe ser una zona válida.',
    'unique' => ':attribute ya ha sido registrado.',
    'uploaded' => 'Subir :attribute ha fallado.',
    'url' => 'El formato :attribute es inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'password' => [
            'min' => 'La :attribute debe contener más de :min caracteres',
        ],
        'email' => [
            'unique' => 'El :attribute ya ha sido registrado.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'nombre',
        'user.name' => 'nombre',
        'username' => 'usuario',
        'email' => 'correo electrónico',
        'user.email' => 'correo electrónico',
        'first_name' => 'nombre',
        'last_name' => 'apellido',
        'lastname' => 'apellido',
        'user.lastname' => 'apellido',
        'password' => 'contraseña',
        'user.password' => 'contraseña',
        'password_confirmation' => 'confirmación de la contraseña',
        'user.password_confirmation' => 'confirmación de la contraseña',
        'city' => 'ciudad',
        'country' => 'país',
        'address' => 'dirección',
        'phone' => 'teléfono',
        'mobile' => 'móvil',
        'age' => 'edad',
        'sex' => 'sexo',
        'gender' => 'género',
        'year' => 'año',
        'month' => 'mes',
        'day' => 'día',
        'hour' => 'hora',
        'minute' => 'minuto',
        'second' => 'segundo',
        'title' => 'título',
        'content' => 'contenido',
        'body' => 'contenido',
        'description' => 'descripción',
        'excerpt' => 'extracto',
        'date' => 'fecha',
        'time' => 'hora',
        'subject' => 'asunto',
        'message' => 'mensaje',
        'lastname' => 'apellido',
        'document' => 'documento de identidad',
        'birth_date' => 'fecha de nacimiento',
        'state' => 'estado/provincia',
        'zip_code' => 'código postal',
        'address_1' => 'dirección 1',
        'address_2' => 'dirección 2',
        'address1' => 'dirección',
        'zipcode' => 'código postal',
        'phone_num' =>  'teléfono',
        'province' => 'provincia',
        'alt_email' => 'correo electrónico alternativo',
        'document_type' => 'tipo de documento',
        'confirm_password' => 'confirmación de la contraseña',
        'user.confirm_password' => 'confirmación de la contraseña',
        'amount_crypto_order' => 'cantidad de criptomoneda',
        'amount_fiat_order' => 'monto',
        'crypto_price_order' => 'precio de la criptomoneda',
        'crypto_id' => 'tipo de criptomoneda',
        'currency_id' => 'tipo de moneda fiat',
        'method' => 'método de pago',
        'payment_email' => 'correo electrónico para el pago',
        'bank_name' => 'nombre del banco',
        'bank_account_number' => 'número de cuenta bancaria',
        'recipients_name' => 'nombre del receptor',
        'recipients_email' => 'correo electrónico del receptor',
        'bank_address' => 'dirección del banco',
        'aba_routing' => 'número de ruta ABA',
        'swift' => 'código SWIFT / BIC',
        'buy_payment_file' => 'comprobante de pago',
        'profile.document' => 'documento de identidad',
        'profile.gender' => 'género',
        'profile.country' => 'país',
        'profile.state' => 'estado/provincia',
        'profile.city' => 'ciudad',
        'profile.zip_code' => 'código postal',
        'profile.address_1' => 'dirección 1',
        'profile.address_2' => 'dirección 2',
        'profile.alt_email' => 'correo electrónico alternativo',
        'profile.document_type.id' => 'tipo de documento',
        'incomePercentage' => 'porcentaje de ganancia',
        'min_sale' => 'mínimo de venta',
        'max_sale' => 'máximo de venta',
        'buyer_name' => 'Nombre',
        'buyer_email' => 'Correo electrónico',
        'points_of_sale' => 'Puntos de venta',
        'old_password' => 'contraseña actual',
        'new_password_confirm' => 'confirmar nueva contraseña',
        'new_password' => 'nueva contraseña',
        'operation_password' => 'clave de operaciones',
        'operation_password_confirm' => 'confirmar clave de operaciones',
        "amount_given" => "monto recargado"
    ],
];
