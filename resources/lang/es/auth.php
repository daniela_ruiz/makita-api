<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'confirm_sent' => 'Te hemos enviado el enlace de confirmación a tu dirección de correo electrónico',
    'confirmed' => 'Tu dirección de correo electrónico ya ha sido confirmada',
    'admin_confirm_sent' => 'Se ha enviado el enlace de confirmación a la dirección de correo electrónico del usuario',
    'admin_confirmed' => 'La dirección de correo electrónico ya ha sido confirmada'
];
