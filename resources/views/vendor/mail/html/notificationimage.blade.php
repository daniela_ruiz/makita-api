<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <h1>{{$notification->title}}</h1>
        </td>
    </tr>
    <tr>
        <td align="center">
            <p>{{$notification->description}}</p>
        </td>
    </tr>
    <tr>
        <td align="center">
            <img src="{{$notification->image}}" alt="{{$notification->title}}">
        </td>
    </tr>
</table>
