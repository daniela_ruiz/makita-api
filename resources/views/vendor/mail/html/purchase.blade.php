<table class="tg">
    <tr>
        <th class="tg-i21v"><span style="font-weight:bold">Producto</span></th>
        <th class="tg-i21v">{{ $purchase->product->name }}</th>
    </tr>
    <tr>
        <td class="tg-i21v"><span style="font-weight:bold">Modelo</span></td>
        <td class="tg-i21v">{{ $purchase->product->model }}</td>
    </tr>
    <tr>
        <td class="tg-i21v"><span style="font-weight:bold">Serial</span></td>
        <td class="tg-i21v">{{ $purchase->serial }}</td>
    </tr>
    <tr>
        <td class="tg-i21v"><span style="font-weight:bold">Fecha de compra</span></td>
        <td class="tg-i21v">{{ $purchase->invoice_date }}</td>
    </tr>
    <tr>
        <td class="tg-i21v"><span style="font-weight:bold">Fecha límite de garantía</span></td>
        <td class="tg-i21v">{{ $purchase->extended_warranty_date }}</td>
    </tr>
</table>

