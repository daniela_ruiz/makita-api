<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <h2>{{$notification->title}}</h2>
        </td>
    </tr>
    <tr>
        <td align="center">
            <p>{{$notification->description}}</p>
        </td>
    </tr>
</table>
