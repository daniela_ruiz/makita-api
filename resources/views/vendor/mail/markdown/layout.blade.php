{!! strip_tags($header) !!}

{!! strip_tags($slot) !!}
@isset($subcopy)

{!! strip_tags($subcopy) !!}
@endisset


@isset($unsubscribe)

    {!! strip_tags($unsubscribe) !!}
@endisset

{!! strip_tags($footer) !!}
