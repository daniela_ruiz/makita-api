@component('mail::warranty')
    {{-- Logo --}}
    @component('mail::logo', ['url' => config('app.url')])
    @endcomponent
    @isset($purchase)
        @component('mail::subcopy', ['purchase' => $purchase])
        @endcomponent
    @endisset
@endcomponent
