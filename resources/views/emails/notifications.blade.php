@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    @if(!empty($notification->image))
        @component('mail::notificationimage', ['notification' => $notification])
        @endcomponent
    @else
        @component('mail::notification', ['notification' => $notification])
        @endcomponent
    @endif

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    @slot('unsubscribe')
        @component('mail::unsubscribe')
            <a href={{ url("unsubscribe/$unsubscribe") }}>Dejar de recibir notificaciones</a>
        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent