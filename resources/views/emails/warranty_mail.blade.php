<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Demystifying Email Design</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>

<body>
<img class="table-bg-image" src="{{ base_path() }}/public/img/background.png"/>
<table border="0" cellpadding="0" cellspacing="0" width="600">

    <tr>
        <td class="content-head">

        </td>
    </tr>

    <tr>
        <td class="content-data">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td width="260" valign="top" style="font-weight: bold">
                        Producto
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>
                    <td width="260" valign="top">
                        {{ $purchase->product->name }}
                    </td>
                </tr>
                <tr>
                    <td width="260" valign="top" style="font-weight: bold">
                        Modelo
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>
                    <td width="260" valign="top">
                        {{ $purchase->product->model }}
                    </td>
                </tr>
                <tr>
                    <td width="260" valign="top" style="font-weight: bold">
                        Serial
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>
                    <td width="260" valign="top">
                        {{ $purchase->serial }}
                    </td>
                </tr>
                <tr>
                    <td width="260" valign="top" style="font-weight: bold">
                        Fecha de compra
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>
                    <td width="260" valign="top">
                        {{ $purchase->invoice_date }}
                    </td>
                </tr>
                <tr>
                    <td width="260" valign="top" style="font-weight: bold">
                        Fecha límite de garantía
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>
                    <td width="260" valign="top">
                        {{ $purchase->extended_warranty_date }}
                    </td>
                </tr>

            </table>

        </td>

    </tr>

    <tr>
        <td class="content-bottom" >
            <div style="margin-left: 3rem;">
                <img src="{{ base_path() }}/public/img/appstore-01.png" height="38">
                <img src="{{ base_path() }}/public/img/googleplay-01.png" height="38">
                <img style="margin-left: 15rem;" src="{{ base_path() }}/public/img/redes-mpa-01.png" height="38">
            </div>
        </td>
    </tr>

</table>

</body>
<style type="text/css">
    img.table-bg-image {
        position: absolute;
        z-index: -1;
    }
    table.with-bg-image, table.with-bg-image tr, table.with-bg-image td {
        background: transparent;
    }
    .content-data{
        padding: 4rem;
        height: 10rem;
        font-family: Calibri, sans-serif;
    }

    .content-head{
        height: 30rem;
    }

    .content-bottom{
        padding-top: 7rem;
    }


</style>

</html>