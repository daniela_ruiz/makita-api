<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title >Makita - Política de Privacidad</title>
    <style type="text/css">
        @page {
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            empty-cells: show
        }

        td, th {
            vertical-align: top;
            font-size: 12pt;
        }

        h1, h2, h3, h4, h5, h6 {
            clear: both;
        }

        ol, ul {
            margin: 0;
            padding: 0;
        }

        li {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        /* "li span.odfLiEnd" - IE 7 issue*/
        li span. {
            clear: both;
            line-height: 0;
            width: 0;
            height: 0;
            margin: 0;
            padding: 0;
        }

        * {
            margin: 0;
        }

        .P11 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0098in;
            line-height: 200%;

        }

        .P12 {

            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0098in;
            line-height: 200%;

        }

        .P13 {

            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0098in;
            line-height: 200%;

            font-weight: bold;
        }

        .P14 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.3126in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.3126in;
        }

        .P15 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.3126in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.3126in;
        }

        .P16 {

            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.3126in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.3126in;

            font-weight: bold;
        }

        .P17 {

            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.3126in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.3126in;
        }

        .P18 {
            font-size: 10pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.3126in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.3126in;
        }

        .P19 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.25in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.25in;
        }

        .P2 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0126in;
            line-height: 200%;

        }

        .P20 {
            font-size: 10pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.25in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.25in;
        }

        .P21 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.3126in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.25in;
        }

        .P22 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.25in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.1874in;
        }

        .P23 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;
            margin-left: 0.1874in;
            margin-right: -0.0126in;
            line-height: 200%;
            text-indent: -0.1874in;
        }

        .P24 {
            font-size: 12pt;
            margin-bottom: 0in;
            margin-left: 0.5in;
            margin-right: -0.0098in;
            margin-top: 0in;
            text-align: justify ! important;
            text-indent: -0.25in;
            writing-mode: lr-tb;
            line-height: 200%;
        }

        .P3 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0126in;
            line-height: 200%;

        }

        .P4 {

            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0126in;
            line-height: 200%;

        }

        .P5 {

            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0126in;
            line-height: 200%;

        }

        .P6 {
            font-size: 10pt;
            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0126in;
            line-height: 200%;

        }

        .P7 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0126in;
            line-height: 200%;

        }

        .P8 {
            font-size: 12pt;
            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0098in;

        }

        .P9 {
            font-size: 10pt;
            text-align: justify ! important;
            writing-mode: lr-tb;

            margin-right: -0.0098in;

        }

        .Footnote_20_Symbol {
            vertical-align: super;
            font-size: 58%;
        }

        .Internet_20_link {
            text-decoration: underline;
        }

        .ListLabel_20_4 {

        }

        .ListLabel_20_5 {

        }

        .T12 {
            font-weight: bold;
        }

        .T13 {
            font-style: italic;
            font-weight: bold;
        }

        .T14 {

        }

        .T15 {
            color: #548dd4;
        }

        .T2 {

            font-weight: bold;
        }

        .T3 {

            font-weight: bold;
        }

        .T4 {

        }

        .T5 {

        }

        .T6 {

            font-style: italic;
            font-weight: bold;
        }

        .T7 {

            font-style: italic;
            font-weight: bold;
        }

        .T8 {

            font-style: italic;
        }

        .T9 {

            font-style: italic;
        }

        /* ODF styles with no properties representable as CSS */
        .ListLabel_20_1 .ListLabel_20_2 .ListLabel_20_3 .page_20_number {
        }
        body {
            color: #000;
            font-family: 'Open Sans', sans-serif;
        }
        .header {
            background: #00909e;
            padding: .5em 4em;
        }

        .container{
            padding: 2em 15%;
        }

        @media (max-width: 767px) {
            .container {
                padding: 2em !important;
            }
        }

        a{
            color: #00909e;
            text-decoration: none;
        }

        h2.title{
            font-family: "Open Sans Condensed",sans-serif;
            font-size: 200%;
            font-weight: bold;
            text-transform: uppercase;
            text-align: center;
        }

        p.ralign {
            text-align: right;
            font-size: smaller !important;
        }

        .header {
            text-align: center;
        }
    </style>
</head>
<body dir="ltr">

<div class="header">
    <img src="/img/makita-logo2.png">
</div>

<div class="container">

<h2 class="title">POLÍTICA DE PRIVACIDAD</h2>
<p class="P4"> </p>
<p class="P2"><span class="T5">Gracias por elegir ser parte de nuestra comunidad  </span><span class="T3">Makita Latin America lnc.</span><span
            class="T5"> ("</span><span class="T3">Compañía</span><span class="T5">", "</span><span
            class="T3">nosotros</span><span class="T5">", "</span><span class="T3">nos</span><span
            class="T5">" o "</span><span class="T3">nuestros</span><span class="T5">"). Nos comprometemos a proteger su información personal y su derecho a la privacidad. Si tiene alguna pregunta o inquietud sobre nuestra política o nuestras prácticas relacionada con su información personal, comuníquese con nosotros a </span><a
            href="mailto:alemus@makitalatinamerica.com" class="ListLabel_20_4"><span class="Internet_20_link"><span
                    class="T5">alemus@makitalatinamerica.com</span></span></a><span class="T5">.</span></p>
<p class="P2"><span class="T5">Cuando visita nuestra aplicación móvil y utiliza nuestros servicios, nos confía su información personal. Nosotros tomamos muy en serio su privacidad. En esta notificación de privacidad, describimos nuestra política de privacidad. Buscamos explicarle de la manera más clara posible qué información recopilamos, cómo la usamos y qué derechos tiene en relación con la misma. Esperamos que se tome un momento para leerlo detenidamente, ya que es importante. Si hay algún término en esta política de privacidad con el que no está de acuerdo, por favor, deje de utilizar nuestras Aplicaciones y nuestros servicios.</span>
</p>
<p class="P2"><span class="T5">Esta política de privacidad se aplica a toda la información recopilada a través de nuestra aplicación móvil ("</span><span
            class="T3">Aplicaciones</span><span class="T5">") y/o cualquier servicio relacionado, ventas, mercadeo o eventos (nos referimos a los mismos conjuntamente en esta política de privacidad como "</span><span
            class="T3">Sitios</span><span class="T5">").</span></p>
<p class="P2"><span class="T3">Por favor, lea esta política de privacidad detenidamente ya que le ayudará a tomar decisiones fundamentadas en relación a compartir su información personal con nosotros.</span>
</p>
<p class="P2"><span class="T3">TABLA DE CONTENIDO</span></p>
    <p class="P2"><span class="T12"><a href="#toc1">1. ¿QUÉ INFORMACIÓN RECOPILAMOS?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc2">2. ¿CÓMO UTILIZAMOS SU INFORMACIÓN?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc3">3. ¿SU INFORMACIÓN SE COMPARTIRÁ CON ALGUIEN?</a></span></p>
    <p class="P2"><a href="#toc4"><span class="T12">4. ¿UTILIZAMOS </span><span class="T13">COOKIES</span><span class="T12"> Y OTRAS TECNOLOGÍAS DE SEGUIMIENTO?</span></a></p>
    <p class="P2"><a href="#toc5"><span class="T12">5. ¿UTILIZAMOS MAPAS GOOGLE </span><span class="T13">(GOOGLE MAPS)?</span></a></p>
    <p class="P2"><span class="T12"><a href="#toc6">6. ¿CÓMO MANEJAMOS SUS ACCESOS SOCIALES?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc7">7. ¿CUÁNTO TIEMPO CONSERVAMOS SU INFORMACIÓN?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc8">8. ¿CÓMO MANTENEMOS SU INFORMACIÓN SEGURA?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc9">9. ¿RECOPILAMOS INFORMACIÓN DE MENORES?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc10">10. ¿CUÁLES SON SUS DERECHOS DE PRIVACIDAD?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc11">11. CONTROLES PARA CARACTERÍSTICAS DE NO SEGUIMIENTO</a></span></p>
    <p class="P15"><span class="T12"><a href="#toc12">12. ¿LOS RESIDENTES DE CALIFORNIA TIENEN DERECHOS ESPECÍFICOS  DE PRIVACIDAD?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc13">13 ¿HACEMOS ACTUALIZACIONES A ESTA POLÍTICA?</a></span></p>
    <p class="P2"><span class="T12"><a href="#toc14">14. ¿CÓMO SE PUEDE CONTACTAR CON NOSOTROS SOBRE ESTA POLÍTICA?</a></span></p>
<p class="P4"> </p>
<p class="P2" id="toc1"><span class="T3">1. ¿QUÉ INFORMACIÓN RECOPILAMOS?</span></p>
<p class="P2"><span class="T3">Información personal que usted nos proporciona</span></p>
<p class="P2"><span class="T7">En resumen:</span><span class="T9"> Nosotros recopilamos la información personal que nos proporciona, como el nombre, dirección,  información de contacto, contraseñas y los datos de seguridad, información de pago y los datos de inicio de sesión en las redes sociales.</span>
</p>
<p class="P2"><span class="T5">Nosotros recopilamos la información personal que usted nos proporciona voluntariamente al registrarse en las Aplicaciones, expresando su interés en obtener información sobre nosotros o nuestros productos y servicios, cuando participa en actividades en las Aplicaciones o de alguna forma, al contactarnos.</span>
</p>
<p class="P2"><span class="T5">La información personal que recopilamos dependerá del contexto de sus interacciones con nosotros y las Aplicaciones, las elecciones que realice y los productos y características que utiliza. La información personal que recopilamos puede incluir lo siguiente:</span>
</p>
<p class="P2"><span class="T3">Nombre y datos de contacto.</span><span class="T5"> Nosotros recopilamos su nombre y apellido, dirección de correo electrónico, dirección postal, número de teléfono y otros datos de contacto similares.</span>
</p>
<p class="P9"> </p>
<p class="P2"><span class="T3">Datos de cuenta.</span><span class="T5"> Nosotros recopilamos contraseñas, sugerencias de contraseñas e información de seguridad similar que usamos para la autenticación y acceso a las cuentas.</span>
</p>
<p class="P2"><span class="T3">Datos de pago</span><span class="T5">. Nosotros recopilamos los datos necesarios para procesar su pago si realiza las compras, como el número de su documento de pago (como el número de su tarjeta de crédito) y el código de seguridad asociado con su documento de pago. Toda la información de pago es almacenada por nuestro procesador de pago y usted debe revisar sus políticas de privacidad y comunicarse directamente con el procesador de pagos para responder sus preguntas.</span>
</p>
<p class="P2"><span class="T3">Datos de acceso a las redes sociales.</span><span class="T5"> Nosotros le brindamos la opción de registrarse utilizando los detalles de la cuenta de las redes sociales, como su cuenta de Facebook, </span><span
            class="T9">Twitter</span><span class="T5"> u otras redes sociales. Si elige registrarse de esta manera, recopilaremos la información descrita en la sección llamada "</span><span
            class="T14">CÓMO MANEJAMOS SUS ACCESOS SOCIALES</span><span class="T5">" a continuación.</span></p>
<p class="P2"><span class="T5">Toda la información personal que nos proporcione debe ser auténtica, completa y precisa, y debe notificarnos cualquier cambio en dicha información personal.</span>
</p>
<p class="P2"><span class="T3">Información recopilada automáticamente</span></p>
<p class="P2"><span class="T7">En resumen:</span><span class="T9"> Cierta información, como la dirección IP y el navegador y las características del dispositivo, se recopila automáticamente cuando visita nuestras Aplicaciones.</span>
</p>
<p class="P2"><span class="T5">Nosotros recopilamos ciertas informaciones automáticamente, cuando usted visita, </span>
</p>
<p class="P2"><span
            class="T5">usa o navega por las Aplicaciones. Esta información no revela su identidad auténtica </span></p>
<p class="P2"><span class="T5">(como su nombre o información de contacto), pero puede incluir información sobre el dispositivo y el uso, como su dirección IP, las características del navegador y el dispositivo, el sistema operativo, las preferencias de idioma,  URL de referencia, nombre del dispositivo, país, la ubicación, información sobre cómo y cuándo utiliza nuestras Aplicaciones y otra información técnica. Esta información se necesita principalmente para mantener la seguridad y el funcionamiento de nuestras Aplicaciones, y para nuestros análisis internos y presentación de informes.  </span>
</p>
<p class="P2"><span class="T5">Como muchas empresas, también recopilamos información a través de </span><span
            class="T9">cookies</span><span class="T5"> y tecnologías similares.</span></p>
<p class="P2"><span class="T3">Información recogida a través de nuestras Aplicaciones.</span></p>
<p class="P2"><span class="T7">En resumen</span><span class="T9">: Es posible que recopilemos información sobre su ubicación geográfica, dispositivo móvil, notificaciones automáticas (push notifications), cuando utilice nuestras Aplicaciones.</span>
</p>
<p class="P2"><span class="T5">Si usa nuestras aplicaciones, también podemos recopilar la siguiente información:</span>
</p>
<p class="P14"><span class="T5">■ </span><span class="T9">Información de Geo-localización</span><span class="T5">. Nosotros podemos solicitar acceso o permiso para realizar un seguimiento de la información basada en la ubicación desde su dispositivo móvil, ya sea de forma continua o mientras utiliza nuestra aplicación </span>
móvil, para proporcionar servicios basados ​​en la ubicación. Si desea cambiar nuestro acceso o permisos, puede hacerlo en la configuración de su dispositivo.</span>
</p>
<p class="P19"><span class="T5">■ </span><span class="T9">Acceso a dispositivos móviles</span><span class="T5">. Es posible que solicitemos acceso o permiso para ciertas funciones desde su dispositivo móvil, incluidas las cuentas de redes sociales, recordatorios y otras funciones de su dispositivo móvil. Si desea cambiar nuestro acceso o permisos, puede hacerlo en la configuración de su dispositivo.</span>
</p>
<p class="P19"><span class="T5">■ </span><span class="T9">Datos del dispositivo móvil</span><span class="T5">. Podemos recopilar automáticamente la información del dispositivo (como la identificación, modelo y fabricante del dispositivo móvil), el sistema operativo, la información de la versión y la dirección IP.</span>
</p>
<p class="P19"><span class="T5">■ </span><span class="T9">Notificaciones Automáticas</span><span class="T5">. Nosotros podemos solicitarle que le enviaremos notificaciones automáticas con relación a su cuenta o la aplicación móvil. Si desea optar por no recibir este tipo de notificaciones, puede desactivarlas en la configuración de su dispositivo.</span>
</p>
<p class="P2"><span class="T3">Información recogida de otras fuentes.</span></p>
<p class="P2"><span class="T7">En resumen:</span><span class="T9"> Es posible que recopilemos datos limitados de bases de datos públicas, socios de mercadeo, plataformas de redes sociales y otras fuentes externas.</span>
</p>
<p class="P2"><span class="T5">Nosotros podemos obtener información sobre usted de otras fuentes, como bases de datos públicas, socios de mercadeo conjuntos, plataformas de redes sociales (como Facebook), así como de otras terceras partes. Los ejemplos de la información que </span>
recibimos de otras fuentes incluyen: información del perfil de las redes sociales (su nombre, sexo, fecha de nacimiento, correo electrónico, ciudad, estado y país actual, números de identificación de usuario de sus contactos, el URL de la imagen de perfil y cualquier otra información que elija hacer pública); clientes potenciales de mercadeo y resultados de búsqueda y enlaces, incluyendo listados pagos (como enlaces de patrocinadores).</span>
</p>
<p class="P2" id="toc2"><span class="T3">2. ¿CÓMO UTILIZAMOS SU INFORMACIÓN?</span></p>
<p class="P2"><span class="T7">En resumen</span><span class="T9">: Nosotros procesamos su información para fines basados ​​en intereses comerciales legítimos, el cumplimiento de nuestro contrato con usted, el cumplimiento de nuestras obligaciones legales y su consentimiento.</span>
</p>
<p class="P2"><span class="T5">Utilizamos la información personal recopilada a través de nuestras Aplicaciones para una variedad de propósitos comerciales que se describen a continuación. Procesamos su información personal para estos fines en función de nuestros intereses comerciales legítimos, para presentarle o celebrar un contrato con usted, con su consentimiento y/o para cumplir con nuestras obligaciones legales. Le indicamos los motivos de procesamiento específicos en los que nos basamos para cada uno de los propósitos enumerados a continuación.</span>
</p>
<p class="P2"><span class="T5">Nosotros usamos la información que recopilamos o recibimos:</span></p>
<p class="P19"><span class="T5">■ </span><span class="T3">Para facilitar la creación de cuentas y el proceso de inicio de sesión.</span><span
            class="T5"> Si elige vincular su cuenta con nosotros a una cuenta de terceros* (como su cuenta de Google o Facebook), utilizamos la información que nos permitió recopilar de esos terceros para facilitar la creación de la cuenta y el proceso de inicio de sesión. Consulte la sección a continuación titulada "</span><span
            class="T14">CÓMO MANEJAMOS SUS ACCESOS SOCIALES</span><span
            class="T5">" para obtener más información.</span></p>
<p class="P21"><span class="T5">■ </span><span
            class="T3">Para enviarle comunicaciones publicitarias y promocionales.</span><span class="T5"> Nosotros y/o nuestros socios de mercadeo de terceros pueden utilizar la información personal que nos envíe para nuestros fines de mercadeo, si esto está de acuerdo con sus preferencias de mercadeo. Puede optar por no recibir nuestros correos electrónicos de mercadeo en cualquier momento (consulte la sección "</span><span
            class="T14">CUÁLES SON SUS DERECHOS DE PRIVACIDAD</span><span class="T5">" a continuación).</span></p>
<p class="P19"><span class="T5">■ </span><span class="T3">Entregar publicidad dirigida a usted</span><span class="T5">. Podemos usar su información para desarrollar y mostrar contenido y publicidad (y trabajar con terceros que lo hagan) adaptados a sus intereses y/o ubicación y para medir su efectividad.</span>
</p>
<p class="P22"><span class="T5">■ </span><span class="T3">Para otros fines comerciales.</span><span class="T5"> Podemos usar su información para otros fines comerciales, como el análisis de datos, la identificación de tendencias del uso, la determinación de la efectividad de nuestras campañas promocionales y para evaluar y mejorar nuestras aplicaciones, productos, servicios, mercadeo y su experiencia.</span>
</p>
<p class="P6"> </p>
<p class="P6"> </p>
<p class="P2" id="toc3"><span class="T3">3. ¿SU INFORMACIÓN SE COMPARTIRÁ CON ALGUIEN?</span></p>
<p class="P2"><span class="T7">En resumen</span><span class="T9">: Nosotros solo compartimos información con su consentimiento, para cumplir con las leyes, para proteger sus derechos, o para cumplir las obligaciones comerciales.</span>
</p>
<p class="P2"><span class="T5">Podemos procesar o compartir datos basado a la siguiente base legal:</span></p>
<p class="P23"><span class="T5">■ </span><span class="T3">Consentimiento:</span><span class="T5"> Nosotros podemos procesar sus datos si nos ha otorgado su consentimiento específico para utilizar su información personal o información en un propósito específico.</span>
</p>
<p class="P19"><span class="T5">■ </span><span class="T3">Intereses legítimos:</span><span class="T5"> Nosotros podemos procesar sus datos cuando sea razonablemente necesario para lograr nuestros intereses comerciales legítimos.</span>
</p>
<p class="P19"><span class="T5">■ </span><span class="T3">Cumplimiento de un contrato:</span><span class="T5"> Cuando hayamos celebrado un contrato con usted, podemos procesar su información personal para cumplir con los términos de nuestro contrato.</span>
</p>
<p class="P23"><span class="T5">■ O</span><span class="T3">bligaciones legales: </span><span class="T5">Nosotros podemos divulgar su información cuando estemos legalmente obligados a hacerlo para cumplir con la ley aplicable, las solicitudes gubernamentales, un procedimiento judicial, una orden judicial o un proceso legal, como en respuesta a una orden o una citación judicial. (incluso en respuesta a las autoridades públicas para cumplir con los requisitos de seguridad nacional o de cumplimiento de la ley).</span>
</p>
<p class="P14"><span class="T5">■ </span><span class="T3">Intereses vitales:</span><span class="T5"> Nosotros podemos divulgar su información cuando creamos que </span>
</p>
<p class="P14"><span class="T5">es necesario investigar, prevenir o tomar medidas con relación a posibles violaciones de nuestras políticas, sospechas de fraude, situaciones que impliquen amenazas potenciales a la seguridad de cualquier persona y actividades ilegales, o como evidencia, en litigios en los que estamos involucrados.</span>
</p>
<p class="P2"><span class="T5">Más específicamente, es posible que necesitemos procesar sus datos o compartir su información personal en las siguientes situaciones:</span>
</p>
<p class="P2"><span class="T5">■ </span><span class="T3">Transferencias de negocios.</span><span class="T5"> Nosotros podemos compartir o transferir su información en relación con, o durante las negociaciones de cualquier fusión, venta de activos de la compañía, financiamiento o adquisición de una parte de nuestro negocio a otra compañía.</span>
</p>
<p class="P2"><span class="T5">■ </span><span class="T3">Anunciantes de terceros.</span><span class="T5"> Nosotros podemos utilizar empresas publicitarias de terceros para publicar anuncios cuando visita las Aplicaciones. Estas compañías pueden usar información sobre sus visitas a nuestro(s) sitio(s) </span><span
            class="T9">web</span><span class="T5"> y otros sitios </span><span class="T9">web</span><span class="T5"> que están contenidos en </span><span
            class="T9">cookies web</span><span class="T5"> y otras tecnologías de seguimiento para proporcionar anuncios sobre bienes y servicios de interés para usted.</span>
</p>
<p class="P2"><span class="T5">■ </span><span class="T3">Paredes de</span><span class="T5"> </span><span class="T3">Oferta.</span><span
            class="T5"> Nuestras aplicaciones pueden mostrar una "pared de ofertas" suministrada por terceros. Dicha pared de oferta permite a los anunciantes de terceros ofrecer dinero virtual, regalos u otros artículos a los usuarios a cambio de la aceptación y finalización de una oferta de publicidad. Dicha pared de oferta puede aparecer en nuestra aplicación móvil y se le mostrará en función de ciertos datos, </span>
como su área geográfica o información demográfica. Al hacer clic en una pared de ofertas, saldrá de nuestra aplicación móvil. Se compartirá un identificador único, como su identificación de usuario, con el proveedor de la pared de ofertas para evitar el fraude y acreditar adecuadamente su cuenta.</span>
</p>
<p class="P11" id="toc4"><span class="T2">4. ¿UTILIZAMOS </span><span class="T6">COOKIES</span><span class="T2"> Y OTRAS TECNOLOGÍAS DE SEGUIMIENTO?</span>
</p>
<p class="P11"><span class="T6">En resumen:</span><span class="T8"> Nosotros podemos usar cookies y otras tecnologías de seguimiento para recopilar y almacenar su información.</span>
</p>
<p class="P11"><span class="T4">Podemos usar </span><span class="T8">cookies</span><span class="T4"> y tecnologías de seguimiento similares (como pequeñas señales (</span><span
            class="T8">web beacons</span><span class="T4">) y  píxeles) para acceder o almacenar información. La información específica sobre cómo usamos dichas tecnologías y cómo puede rechazar ciertas</span><span
            class="T8"> cookies</span><span class="T4"> se encuentra en nuestra Política de </span><span class="T8">Cookies</span><span
            class="T4">.</span></p>
<p class="P11" id="toc5"><span class="T2">5. ¿UTILIZAMOS MAPAS DE GOOGLE </span><span class="T6">(GOOGLE MAPS)</span><span
            class="T2">?</span></p>
<p class="P11"><span class="T8">En resumen: Sí, nosotros utilizamos Mapas de Google (Google Maps) con el fin de brindar un mejor servicio.</span>
</p>
<p class="P11"><span class="T4">Este sitio web, la aplicación móvil o aplicación de Facebook utiliza las Aplicaciones de Mapas de Google (</span><span
            class="T8">Google Maps</span><span class="T4">). Puede encontrar los Términos de servicio de la Aplicación de Mapas de Google (</span><span
            class="T8">Google Maps)</span><span class="T4"> </span><span class="T15">here</span><span class="T4">. Para comprender mejor la Política de privacidad de Google, consulte este enlace (</span><span
            class="T15">link</span><span class="T4">).</span></p>
<p class="P11"><span class="T4">Al utilizar nuestra implementación de la API de Mapas, usted acepta estar sujeto a los Términos de servicio de Google. Usted acepta permitirnos obtener o almacenar en caché su ubicación. Puede revocar su consentimiento en cualquier momento. Nosotros utilizamos la información sobre la ubicación junto con los datos de otros proveedores de datos.</span>
</p>
<p class="P11" id="toc6"><span class="T2">6. ¿CÓMO MANEJAMOS SUS ACCESOS SOCIALES?</span></p>
<p class="P11"><span class="T6">En resumen:</span><span class="T8"> Si decide registrarse o iniciar sesión en nuestros sitios web con una cuenta de redes sociales, es posible que tengamos acceso a cierta información sobre usted.</span>
</p>
<p class="P11"><span class="T4">Nuestras aplicaciones le ofrece la posibilidad de registrarse e iniciar sesión utilizando los datos de su cuenta de redes sociales de terceros (como sus inicios de sesión de Facebook o </span><span
            class="T8">Twitter</span><span class="T4">). Cuando elija hacer esto, recibiremos cierta información de perfil sobre usted de parte de su proveedor de redes sociales. La información de perfil que recibimos puede variar según el proveedor de redes sociales relacionado, pero a menudo incluirá su nombre, dirección de correo electrónico, lista de amigos, foto de perfil y otra información que elija hacer pública.</span>
</p>
<p class="P11"><span class="T4">Nosotros utilizaremos la información que recibimos sólo para los fines que se describen en esta política de privacidad o que de lo contrario se explican claramente en las Aplicaciones. Tenga en cuenta que no controlamos, y no somos responsables de, otros usos de su información personal por parte de un proveedor de redes </span>
sociales de terceros. Le recomendamos que revise su política de privacidad para comprender cómo recopilan, utilizan y comparten su información personal y cómo puede configurar sus preferencias de privacidad en sus sitios y aplicaciones.</span>
</p>
<p class="P11" id="toc7"><span class="T2">7. ¿CUÁNTO TIEMPO CONSERVAMOS SU INFORMACIÓN?</span></p>
<p class="P11"><span class="T6">En resumen</span><span class="T8">: Nosotros conservamos su información durante el tiempo que sea necesario para cumplir los fines descritos en esta política de privacidad, a menos que la ley exija lo contrario.</span>
</p>
<p class="P11"><span class="T4">Nosotros sólo conservamos su información personal durante el tiempo que sea necesario para los fines establecidos en esta política de privacidad, a menos que la ley exija o permita un período de conservación más prolongado (como impuestos, contabilidad u otros requisitos legales). Ningún propósito de esta política nos obligará a conservar su información personal durante más tiempo que el período en que los usuarios tienen una cuenta con nosotros.</span>
</p>
<p class="P11"><span class="T4">Cuando no tengamos una necesidad comercial legítima en curso para procesar su información personal, la eliminaremos o anonimizaremos o, si esto no es posible (por ejemplo, porque su información personal ha sido almacenada en archivos de respaldo), entonces su información personal será almacenada de forma segura y la separaremos de cualquier procesamiento posterior hasta que sea posible su eliminación.</span>
</p>
<p class="P11" id="toc8"><span class="T2">8. ¿CÓMO MANTENEMOS SU INFORMACIÓN SEGURA?</span></p>
<p class="P11"><span class="T6">En resumen:</span><span class="T8"> Nuestro objetivo es proteger su información personal, a través de un sistema de medidas de seguridad técnicas y organizativas.</span>
</p>
<p class="P11"><span class="T4">Hemos implementado medidas de seguridad técnicas y organizativas apropiadas diseñadas para proteger la seguridad de cualquier información personal que procesamos. Sin embargo, también recuerde que no podemos garantizar que Internet en sí sea 100% segura. Aunque haremos todo lo posible para proteger su información personal, la transmisión de información personal hacia y desde nuestras Aplicaciones es por su propia cuenta. Solo debe acceder a los servicios dentro de un entorno seguro.</span>
</p>
<p class="P11" id="toc9"><span class="T2">9. ¿RECOPILAMOS INFORMACIÓN DE MENORES?</span></p>
<p class="P11"><span class="T6">En resumen</span><span class="T8">: Nosotros no recopilamos, conscientemente, datos de, ni comercializamos con niños menores de 18 años.</span>
</p>
<p class="P11"><span class="T4">Nosotros no solicitamos, conscientemente, datos o mercadeo de niños menores de 18 años. Al usar las Aplicaciones, usted declara que tiene al menos, 18 años o que es el padre o tutor de dicho menor y está de acuerdo que dicho menor dependiente, use las Aplicaciones. Si nos enteramos de que se ha recopilado información personal de usuarios menores de 18 años, desactivaremos la cuenta y tomaremos las medidas razonables para eliminar rápidamente dichos datos de nuestros registros. Si usted se </span>
entera de cualquier información que hayamos recopilado de niños menores de 18 años, por favor, contáctenos a _______________</span>
</p>
<p class="P11" id="toc10"><span class="T2">10. ¿CUÁLES SON SUS DERECHOS DE PRIVACIDAD?</span></p>
<p class="P11"><span class="T6">En resumen</span><span class="T8">: Usted puede revisar, cambiar o cancelar su cuenta en cualquier momento.</span>
</p>
<p class="P11"><span class="T4">Si usted reside en el Espacio Económico Europeo y cree que estamos procesando ilegalmente su información personal, también tiene derecho de presentar una queja ante la autoridad de supervisión de protección de datos local. Usted puede encontrar los detalles de contacto aquí: </span><a
            href="http://ec.europa.eu/justice/data-ptection/bodies/authorities/index_en.htm" target="_blank"
            ><span class="Internet_20_link"><span class="T4">http://ec.europa.eu/justice/data-ptection/bodies/authorities/index_en.htm</span></span></a>
</p>
<p class="P13"> </p>
<p class="P11"><span class="T2">Información de la cuenta</span></p>
<p class="P11"><span class="T4">Si en algún momento desea revisar o cambiar la información en su cuenta o cancelar su cuenta, usted puede:</span>
</p>
<p class="P11"><span
            class="T4">■ Iniciar la sesión en la configuración de su cuenta y actualizar su cuenta de usuario.</span>
</p>
<p class="P11"><span class="T4">A su solicitud para finalizar su cuenta, desactivaremos o eliminaremos su cuenta y su información de nuestras bases de datos activas. Sin embargo, se puede retener cierta información en nuestros archivos para evitar fraudes, solucionar problemas, </span>
ayudar con cualquier investigación, hacer cumplir nuestros Términos de uso y/o cumplir con los requisitos legales.</span>
</p>
<p class="P11"><span class="T2">Eliminación de mercadeo por correo electrónico:</span><span class="T4"> Usted puede cancelar la suscripción de nuestra lista de correo electrónico de mercadeo en cualquier momento haciendo clic en el enlace para cancelar la suscripción que se encuentra en los correos electrónicos que enviamos o poniéndose en contacto con nosotros a través de los detalles que se suministran a continuación. Usted será eliminado de la lista de correo electrónico de mercadeo. No obstante, aún tendremos que enviarle correos electrónicos relacionados con el servicio que sean necesarios para la administración y el uso de su cuenta. De lo contrario, puede:</span>
</p>
<p><span class="T4">Tenga en cuenta sus preferencias cuando registre una cuenta con el sitio. </span></p>
<p class="P11" id="toc11"><span class="T2">11. CONTROLES PARA CARACTERÍSTICAS DE NO SEGUIMIENTO.</span></p>
<p class="P11"><span class="T4">La mayoría de los navegadores web y algunos sistemas operativos móviles y aplicaciones móviles incluyen una función de "No seguimiento" ("DNT") o configuración que usted puede activar para indicar su preferencia de privacidad de no tener datos sobre sus actividades de navegación en línea monitoreadas y recopiladas. No se ha finalizado ningún estándar de tecnología uniforme para reconocer e implementar señales DNT. Por lo cual, actualmente no respondemos a las señales del navegador DNT ni a ningún otro mecanismo que informe automáticamente, su elección de no ser seguido en línea. Si se adopta un estándar </span>
para el seguimiento en línea que debemos seguir en el futuro, le informaremos sobre esa práctica en una versión revisada de esta Política de Privacidad.</span>
</p>
<p class="P15" id="toc12"><span class="T3">12. ¿LOS RESIDENTES DE CALIFORNIA TIENEN DERECHOS ESPECÍFICOS DE PRIVACIDAD?</span></p>
<p class="P3"><span class="T7">En resumen:</span><span class="T9">  </span><span class="T8">Sí</span><span class="T9">, si usted es residente de California, se le otorgan ciertos derechos de privacidad específicos en relación al acceso a su información personal. </span>
</p>
<p class="P3"><span class="T5">La Sección 1798.83 del Código Civil de California, también conocida como la ley</span>
    <span class="T3"> </span><span class="T5">"</span><span class="T9">Shine The Light",</span><span class="T5"> permite que nuestros usuarios residentes de California nos soliciten y obtengan, una vez al año y de forma gratuita, información sobre las categorías de información personal (si hubiese) divulgada a terceros con fines de mercadeo directo y el nombre y la dirección de todas las terceras partes  con las que compartimos información personal en el año calendario inmediatamente anterior. Si usted es un residente de California y le gustaría hacer una solicitud de esta clase, envíela por escrito utilizando la información de contacto que se proporciona a continuación.</span>
</p>
<p class="P3"><span class="T5">Si es menor de 18 años, reside en California y tiene una cuenta registrada con las Aplicaciones, tiene derecho a solicitar la eliminación de datos no deseados que publique públicamente en las aplicaciones. Para solicitar la eliminación de dichos datos, comuníquese con nosotros utilizando la información de contacto que se proporciona a continuación, e incluya la dirección de correo electrónico asociada </span>
<span class="T5">con su cuenta y una declaración de que reside en California. Nos aseguraremos de que los datos no se divulguen públicamente en las aplicaciones, pero tenga en cuenta que es posible que los datos no se eliminen de forma completa o en su totalidad de nuestros sistemas.</span>
</p>
<p class="P2" id="toc13"><span class="T3">13 ¿HACEMOS ACTUALIZACIONES A ESTA POLÍTICA?</span></p>
<p class="P11"><span class="T6">En resumen</span><span class="T8">: Sí, nosotros actualizaremos esta política según sea necesario para cumplir con las leyes aplicables.</span>
</p>
<p class="P11"><span class="T4">Nosotros podemos actualizar esta política de privacidad de vez en cuando. La versión actualizada se indicará con una fecha "Revisada" actualizada y la versión actualizada entrará en vigencia tan pronto como esté accesible. Si realizamos cambios sustanciales a esta política de privacidad, podemos notificarle mediante la publicación de un aviso prominente de dichos cambios o enviándole directamente una notificación. Le recomendamos que revise esta política de privacidad con frecuencia para estar informado de cómo protegemos su información.</span>
</p>
<p class="P11" id="toc14"><span class="T2">14. ¿CÓMO SE PUEDE CONTACTAR CON NOSOTROS SOBRE ESTA POLÍTICA?</span></p>
<p class="P11"><span class="T4">Si tiene preguntas o comentarios sobre esta póliza, puede enviarnos un correo electrónico a info@makitalatinamerica.com o por correo a:  </span></p>
<p>&nbsp;</p>

<p class="P11"><span class="T4">Makita Latín America lnc. </span></p>
<p class="P11"><span class="T4">10801 NW 97 Street</span></p>
<p class="P11"><span class="T4">Suite 13</span></p>
<p class="P11"><span class="T4">Miami, FL 33178</span></p>
<p class="P11"><span class="T4">Estados Unidos</span></p>

<p class="ralign">Última actualización: 1 de abril de 2019</p>

</div>
</body>
</html>