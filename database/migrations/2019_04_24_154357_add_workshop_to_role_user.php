<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkshopToRoleUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      Schema::table('role_user', function (Blueprint $table) {
        $table->integer("workshop_id")->nullable()->unsigned();

        $table->foreign("workshop_id")->references("id")->on("workshops");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
      Schema::table('benefit_customer', function (Blueprint $table) {
        $table->dropColumn("workshop_id");
      });
    }
}
