<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricePurchaseToBenefitCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('benefit_customer', function (Blueprint $table) {
        $table->decimal('price')->nullable();
        $table->integer('purchase_id')->unsigned()->nullable();

        $table->foreign('purchase_id')->references('id')->on('purchases');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('credit_notes', function (Blueprint $table) {
        $table->dropColumn('price');
        $table->dropColumn('purchase_id');
      });
    }
}
