<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefit_customer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('benefit_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('workshop_id')->unsigned();
            $table->boolean('approved')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('benefit_id')->references('id')->on('benefits');
            $table->foreign('workshop_id')->references('id')->on('workshops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('benefit_customer', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropForeign(['benefit_id']);
        });
        Schema::dropIfExists('benefit_customer');
    }
}
