<?php
use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'birth_date' => $faker->date('Y-m-d', 'now'),
        'gender' => $faker->randomElement(
            array ('Masculino','Femenino','masculino','femenino','m','f','M','F','Otro','otro')
        ),
    ];
});
