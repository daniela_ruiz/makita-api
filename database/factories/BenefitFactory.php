<?php

use App\Manufacturer;
use App\Product;
use App\Benefit;
use Faker\Generator as Faker;

$factory->define(Benefit::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text(100),
    ];
});
