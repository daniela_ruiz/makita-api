<?php

use App\Customer;
use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(\App\Purchase::class, function (Faker $faker, $attributes) {
    return [
        'invoice_number' => $faker->ean8,
        'invoice_date' => Carbon::createFromTimestamp($faker->dateTime($max = 'now', $timezone = null)
            ->getTimestamp())->toDateString(),
        'customer_id' => $attributes['customer_id'],
        'points' => $faker->numberBetween(1, 2000)
    ];
});
