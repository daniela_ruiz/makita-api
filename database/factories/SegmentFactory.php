<?php

use App\Segment;
use Faker\Generator as Faker;

$factory->define(Segment::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
