<?php

use App\Category;
use App\Role;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker, $attributes) {
    return [
        'name' => $attributes['name'],
        'description' => $attributes['description']
    ];
});
