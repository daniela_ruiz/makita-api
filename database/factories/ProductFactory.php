<?php

use App\Manufacturer;
use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text(15),
        'points' => $faker->numberBetween(1, 50),
        'model' => $faker->word,
        'warranty' => $faker->numberBetween(1, 30),
        'service_time' => $faker->numberBetween(1, 13),
        'warranty_time' => $faker->randomElement(Product::getEnum('warranty_time')),
        'sku' => $faker->ean13
    ];
});
