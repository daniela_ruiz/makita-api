<?php

use App\Manufacturer;
use App\Product;
use Faker\Generator as Faker;

$factory->define(\App\Invoice::class, function (Faker $faker) {
    return [
        'number' => $faker->ean8,
        'date' => \Carbon\Carbon::createFromTimestamp($faker->dateTime($max = 'now', $timezone = null)->getTimestamp())
            ->toDateString(),
    ];
});
