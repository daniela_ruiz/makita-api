<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = factory(App\Category::class, 10)->create();
        $segments = factory(App\Segment::class, 10)->create();

        for ($i=0; $i<100; $i++) {
            $product = factory(App\Product::class)->make();
            $product->category()->associate($categories->random());
            $product->segment()->associate($segments->random());
            $product->save();
        }
    }
}
