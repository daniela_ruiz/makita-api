<?php

use App\Configuration;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuration::firstOrCreate(
            [
                'name' => 'extended_warranty'
            ],
            [
                'value' => '6',
                'type' => 'integer',
                'description' => 'Tiempo de garantia extendida'
            ]
        );
        Configuration::firstOrCreate(
            [
                'name' => 'extended_warranty_time'
            ],
            [
                'value' => 'M',
                'type' => 'time',
                'description' => 'Unidad de tiempo de garantia extendida (A, M, D)'
            ]
        );
        Configuration::firstOrCreate(
            [
                'name' => 'notification_interval'
            ],
            [
                'value' => '5',
                'type' => 'integer',
                'description' => 'Intervalo de días para notificar mantenimiento'
            ]
        );
    }
}
