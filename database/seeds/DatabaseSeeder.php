<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfigurationTableSeeder::class);

        if (\App\Role::all()->isEmpty()) {
            $this->call(RoleTableSeeder::class);
            $this->call(NewRolesTableSeeder::class);
        }
        if (\App\User::all()->isEmpty()) {
            $this->call(UserAdminSeeder::class);
        }
        if (\App\Store::all()->isEmpty()) {
            $this->call(StoresTableSeeder::class);
        }
        if (\App\Level::all()->isEmpty()) {
            $this->call(LevelTableSeeder::class);
        }
        /*
        if (\App\Product::all()->isEmpty()) {
            $this->call(ProductsTableSeeder::class);
        }
        if (\App\Workshop::all()->isEmpty()) {
            $this->call(WorkshopsTableSeeder::class);
        }
        if (\App\Benefit::all()->isEmpty()) {
            $this->call(BenefitsTableSeeder::class);
        }*/
    }
}
