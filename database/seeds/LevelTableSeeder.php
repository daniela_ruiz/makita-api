<?php

use App\Level;
use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::create([
            'name' => 'Gold',
            'slug' => 'gold',
            'points' => 0]);
        Level::create([
            'name' => 'Platinum',
            'slug' => 'platinum',
            'points' => 300,
            'multiplier' => 2]);
        Level::create([
            'name' => 'VIP',
            'slug' => 'vip',
            'points' => 900,
            'multiplier' => 3]);
    }
}
