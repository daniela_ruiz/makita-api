<?php
use Illuminate\Database\Seeder;
use App\Role;

class NewRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'accountant',
            'description' => 'Accountant']);
        Role::create([
            'name' => 'manager',
            'description' => 'Manager']);
        Role::create([
          'name' => 'marketing',
          'description' => 'Marketing Content']);
    }
}
