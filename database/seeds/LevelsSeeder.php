<?php

use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = \App\Customer::all();

        foreach ($customers as $customer) {
            $customer->level()->associate($customer->getLevel());
            $customer->save();
        }
    }
}
