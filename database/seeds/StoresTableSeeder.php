<?php

use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stores = [
            [
                "title" => "CENTRO DE SERVICIO DE FÁBRICA MAKITA",
                "lat" => "8.9737876",
                "lng" => "-79.5613749",
            ],
            [
                "title" => "SERVICIOS Y CONSTRUCCIONES GUERRA #1",
                "lat" => "8.431833",
                "lng" => "-82.437917",
            ],
            [
                "title" => "SERVICIOS Y CONSTRUCCIONES GUERRA #2",
                "lat" => "8.431417",
                "lng" => "-82.438500",
            ],
            [
                "title" => "SERVICENTRO ECONO HOGAR",
                "lat" => "9.000222",
                "lng" => "-79.526250",
            ],
            [
                "title" => "SERVICENTRO ECONO HOGAR",
                "lat" => "8.927944",
                "lng" => "-79.716000",
            ],
            [
                "title" => "ALMACEN Y FERRETERIA LG",
                "lat" => "8.928139",
                "lng" => "-79.705306",
            ],
            [
                "title" => "CDISA (Compañia de Desarrollo Industrial, S.A.)",
                "lat" => "9.034889",
                "lng" => "-79.460889",
            ],
            [
                "title" => "MEGA SERVICIOS Y ALQUILERES, S.A.",
                "lat" => "9.045500",
                "lng" => "-79.506500",
            ],
            [
                "title" => "REPARACIONES ACEVEDO",
                "lat" => "9.014611",
                "lng" => "-79.500333",
            ],
            [
                "title" => "SERVICIOS ELECTRÓNICOS BJT",
                "lat" => "8.099333",
                "lng" => "-80.981194",
            ],
            [
                "title" => "INVERSIONES ASOREC, S.A.",
                "lat" => "9.011389",
                "lng" => "-79.521306",
            ],
            [
                "title" => "MULTISERVICIOS MARIO #1",
                "lat" => "7.970139",
                "lng" => "-80.428806",
            ],
            [
                "title" => "MULTISERVICIOS MARIO #2",
                "lat" => "7.970139",
                "lng" => "-80.428806",
            ],
            [
                "title" => "SERVICIOS ELECTRO-INDUSTRIALES",
                "lat" => "8.429944",
                "lng" => "-82.432806",
            ],
            [
                "title" => "ELECTRÓNICA SAN ANDRESITO",
                "lat" => "9.354639",
                "lng" => "-79.901806",
            ],
            [
                "title" => "S.T ELECTRIMOTORES",
                "lat" => "7.970139",
                "lng" => "-80.428806",
            ],
            [
                "title" => "A.L. MOTORS",
                "lat" => "8.517750",
                "lng" => "-82.621111",
            ],
            [
                "title" => "VENTAS Y SERVICIOS MAYAMI",
                "lat" => "8.246333",
                "lng" => "-80.543250",
            ],
            [
                "title" => "SERVICIOS TONY",
                "lat" => "9.024722",
                "lng" => "-79.504889",
            ],
            [
                "title" => "SERVICENTRO MADGA",
                "lat" => "9.041722",
                "lng" => "-79.458694",
            ],
            [
                "title" => "MEGA SERVICES RENTAL",
                "lat" => "8.925333",
                "lng" => "-79.712083",
            ],
            [
                "title" => "RESHELECT",
                "lat" => "9.080111",
                "lng" => "-79.405389",
            ],
            [
                "title" => "MULTIREPUESTOS MUNNSA",
            ],
            [
                "title" => "OTRA",
            ]
        ];
        foreach ($stores as $store) {
            \App\Store::create($store);
        }
    }
}
