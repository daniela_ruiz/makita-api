<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class BenefitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Benefit::class, 20)->create()->each(function ($benefit) {
            $benefit->level()->associate(\App\Level::inRandomOrder()->first())->save();
        });
    }
}
