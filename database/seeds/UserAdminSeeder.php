<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->create([
            'name' => 'Admin',
            'email' => 'dev@grupoapok.com',
            'password' => \Illuminate\Support\Facades\Hash::make('secret')
        ]);
        $user->roles()->attach(Role::findByName('admin'));
    }
}
