<?php
use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'admin',
            'description' => 'Administrator']);
        Role::create([
            'name' => 'operator',
            'description' => 'Operator']);
        Role::create([
            'name' => 'customer',
            'description' => 'Customer']);
    }
}
