<?php

namespace Tests\Traits;

trait ApiTestTrait
{
    public function assertApiResponse(Array $expectedData, $response, $code = 200)
    {
        unset($expectedData['updated_at']);
        $result = [
            'data' => $expectedData
        ];
        $response->assertStatus($code)->assertJson($result);
    }

    public function assertPaginatedResponse($expectedData, $response)
    {
        $object = $expectedData[0];
        $expectedStructure = (array_keys($object->attributesToArray()));
        $structure = [
            "data" => [
                '*' => $expectedStructure
            ],
            "links" => [
                "first",
                "last",
                "prev",
                "next"
            ],
            "meta" => [
                "current_page",
                "from",
                "last_page",
                "path",
                "per_page",
                "to",
                "total"
            ]
        ];

        $response->assertJsonStructure($structure);
    }
}
