<?php

namespace Tests\Feature\Manufacturers;

use App\Category;
use App\Manufacturer;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class CRUDManufacturersTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $postData = [
            'name' => 'ACME'
        ];
        $result = [
            'id' => 1,
            'name' => 'ACME'
        ];
        $res = $this->postJson("api/manufacturers", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $manufacturer = factory(Manufacturer::class)->create();
        $res = $this->get("api/manufacturers/$manufacturer->id");
        $this->assertApiResponse($manufacturer->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $manufacturers = factory(Manufacturer::class, 5)->create();
        $res = $this->get("api/manufacturers");
        $this->assertPaginatedResponse($manufacturers, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $manufacturer = factory(Manufacturer::class)->create();
        $putData = [
            'name' => 'Edit'
        ];
        $result = [
            'id' => 1,
            'name' => $putData['name']
        ];
        $res = $this->putJson("api/manufacturers/$manufacturer->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $manufacturer = factory(Manufacturer::class)->create();
        $this->delete("api/manufacturers/$manufacturer->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
