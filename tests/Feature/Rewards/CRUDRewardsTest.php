<?php

namespace Tests\Feature\Rewards;

use App\Benefit;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class CRUDRewardsTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $postData = [
            'description' => '25% Off Coupon',
            'points' => 50,
        ];
        $result = [
            'id' => 1,
            'description' => $postData['description'],
            'points' => $postData['points'],
        ];
        $res = $this->postJson("api/rewards", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $reward = factory(Benefit::class)->create();
        $res = $this->get("api/rewards/$reward->id");
        $this->assertApiResponse($reward->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $rewards = factory(Benefit::class, 5)->create();

        $res = $this->get("api/rewards?filter=i");

        $this->assertPaginatedResponse($rewards, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $reward = factory(Benefit::class)->create();
        $putData = [
            'description' => 'Edited description'
        ];
        $result = $reward->toArray();
        $result['description'] = $putData['description'];
        $res = $this->putJson("api/rewards/$reward->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $reward = factory(Benefit::class)->create();
        $this->delete("api/rewards/$reward->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
