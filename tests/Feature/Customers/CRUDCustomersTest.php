<?php

namespace Tests\Feature\Customers;

use App\Customer;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class CRUDCustomersTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make()->getAttributes());

        $res = $this->get("api/customers/$customer->id");

        $this->assertApiResponse($customer->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $customers = factory(User::class, 5)->create()->each(function ($p) {

            $p->profile()->create(factory(Customer::class)->make()->getAttributes());
        });
        $res = $this->get("api/customers?filter=i");

        $this->assertPaginatedResponse($customers, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make()->getAttributes());
        $putData = [
            'name' => 'Edited Name',
            'birth_date' => '2017-12-12'
        ];

        $expectedData = array_merge($customer->profile->getAttributes(), $customer->attributesToArray());

        $expectedData['name'] = $putData['name'];
        $expectedData['birth_date'] = $putData['birth_date'];

        $res = $this->putJson("api/customers/$customer->id", $putData);

        $this->assertApiResponse($expectedData, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make()->getAttributes());
        $this->delete("api/customers/$customer->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
