<?php

namespace Tests\Feature\Customers;

use App\Category;
use App\Customer;
use App\Product;
use App\Purchase;
use App\Redemption;
use App\Benefit;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class OperationsCustomersTest extends TestCase
{
    use ApiTestTrait;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadOperations()
    {
        $user = factory(User::class)->create();
        $user->profile()->create(factory(Customer::class)->make(['points' => 0])->getAttributes());
        $user->roles()->attach(Role::findByName('customer'));
        Passport::actingAs(
            $user
        );
        $products = factory(Product::class, 2)->create(['points' => 200])->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });
        $purchases = [];
        for ($i = 0; $i < 3; $i++) {
            $purchase = factory(Purchase::class)->create(['customer_id' => $user->profile->id,
                'points' => $products->sum('points')]);
            $purchase->products()->attach($products->pluck('id'));
            unset($purchase['customer_id']);
            $purchases[] = $purchase;
        }

        $rewards = [];
        for ($i=0; $i<5; $i++) {
            $reward = factory(Benefit::class)->create(['points' => 50]);

            $redemption = new Redemption();
            $redemption->reward_id = $reward->id;
            $redemption->customer_id = $user->profile->id;
            $redemption->points = $reward->points;
            $redemption->save();
            unset($redemption->customer_id);
            unset($redemption->reward_id);
            $rewards[] = $redemption;
        }
        $total = collect($purchases)->merge(collect($rewards))->toArray();
        $res = $this->get("api/operations")->decodeResponseJson();
        $this->assertEquals(950, $res['meta']['points']);
        $this->assertSameSize($total, $res['data']);
    }
}
