<?php

namespace Tests\Feature\Products;

use App\Category;
use App\Manufacturer;
use App\Product;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class CRUDProductsTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $manufacturer = factory(Manufacturer::class)->create();
        $postData = [
            'name' => 'Product',
            'points' => 50,
            'warranty' => 20,
            'time' => 'M',
            'sku' => 'NIKE-PEG-NE-43',
            'manufacturer_id' => $manufacturer->id,
        ];
        $result = [
            'id' => 1,
            'name' => 'Product',
            'points' => 50,
            'warranty' => 20,
            'time' => 'M',
            'sku' => 'NIKE-PEG-NE-43',
            'manufacturer_id' => $manufacturer->id,
        ];
        $res = $this->postJson("api/products", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $product = factory(Product::class)->create();
        $res = $this->get("api/products/$product->id");
        $this->assertApiResponse($product->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $products = factory(Product::class, 5)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });

        $res = $this->get("api/products?filter=i");

        $this->assertPaginatedResponse($products, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $product = factory(Product::class)->create();
        $putData = [
            'name' => 'Edit'
        ];
        $result = $product->toArray();
        $result['name'] = $putData['name'];
        $res = $this->putJson("api/products/$product->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $product = factory(Product::class)->create();
        $this->delete("api/products/$product->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
