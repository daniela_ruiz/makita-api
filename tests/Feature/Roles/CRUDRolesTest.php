<?php

namespace Tests\Feature\Roles;

use App\Category;
use App\Manufacturer;
use App\Product;
use App\Role;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class CRUDRolesTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $postData = [
            'name' => 'New Role',
            'description' => 'New Role',
        ];
        $result = [
            'id' => Role::all()->count() + 1,
            'name' => 'New Role',
            'description' => 'New Role',
        ];
        $res = $this->postJson("api/roles", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $role = factory(Role::class)->create([
            'name' => 'Admin',
            'description' => 'Administrator',
        ]);
        $res = $this->get("api/roles/$role->id");
        $this->assertApiResponse($role->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $roles = Role::all();

        $res = $this->get("api/roles?filter=i");

        $this->assertPaginatedResponse($roles, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $role = factory(Role::class)->create([
            'name' => 'admin',
            'description' => 'Administrator',
        ]);
        $putData = [
            'name' => 'edit'
        ];
        $result = $role->toArray();
        $result['name'] = $putData['name'];
        $res = $this->putJson("api/roles/$role->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $role = factory(Role::class)->create([
            'name' => 'admin',
            'description' => 'Administrator',
        ]);
        $this->delete("api/roles/$role->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
