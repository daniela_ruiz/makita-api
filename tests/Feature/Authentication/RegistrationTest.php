<?php

namespace Tests\Feature\Users;

use Carbon\Carbon;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class RegistrationTest extends TestCase
{
    use ApiTestTrait;
    /**
     * @test
     * Register test
     * @return void
     */
    public function canRegister()
    {
        $pass = $this->faker->password;

        $postData = [
            'name' => $this->faker->name,
            'email' => $this->faker->freeEmail,
            'password' => $pass,
            'password_confirmation' => $pass,
            'birth_date' => $this->faker->date('Y-m-d', 'now'),
            'gender' => $this->faker->randomElement(
                array ('Masculino','Femenino','masculino','femenino','m','f','M','F','Otro','otro')
            ),
        ];

        $result = $postData;

        unset($result['password']);
        unset($result['password_confirmation']);

        $res = $this->postJson("api/users/register", $postData);

        $this->assertApiResponse($result, $res, 201);
    }
}
