<?php

namespace Tests\Feature\Users;

use App\Customer;
use App\User;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class UserProfileTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadProfile()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make()->getAttributes());
        Passport::actingAs($customer);

        $expectedData = array_merge($customer->profile->getAttributes(), $customer->attributesToArray());

        $res = $this->get("api/users/me");

        $this->assertApiResponse($expectedData, $res);
    }

    public function testUpdateProfile()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make()->getAttributes());
        Passport::actingAs($customer);
        $putData = [
            'name' => 'Edited Name',
            'birth_date' => '2010-12-12'
        ];

        $expectedData = array_merge($customer->profile->getAttributes(), $customer->attributesToArray());

        $expectedData['name'] = $putData['name'];
        $expectedData['birth_date'] = $putData['birth_date'];

        $res = $this->putJson("api/users/me", $putData);

        $this->assertApiResponse($expectedData, $res);
    }

    public function testUpdatePassword()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make()->getAttributes());
        Passport::actingAs($customer);
        $putData = [
            'password' => 'newPassword',
            'password_confirmation' => 'newPassword'
        ];

        $this->putJson("api/users/change-password", $putData)->assertStatus(200);
    }
}
