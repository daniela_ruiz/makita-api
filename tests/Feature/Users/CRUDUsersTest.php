<?php

namespace Tests\Feature\Users;

use App\Role;
use App\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class CRUDUsersTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $admin = factory(Role::class)->create(
            [
                'name' => 'admin',
                'description' => 'Administrator',
            ]
        );
        $op = factory(Role::class)->create(
            [
                'name' => 'operator',
                'description' => 'Operator',
            ]
        );
        $postData = [
            'name' => 'User Test',
            'email' => 'example@email.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
            'roles' => [$admin->id, $op->id]
        ];
        $result = [
            'id' => User::all()->count() + 1,
            'name' => 'User Test',
            'email' => 'example@email.com',
        ];
        $res = $this->postJson("api/users", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $user = factory(User::class)->create();
        $res = $this->get("api/users/$user->id");
        $this->assertApiResponse($user->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $users = factory(User::class, 3)->create();

        $res = $this->get("api/products?filter=i");

        $this->assertPaginatedResponse($users, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $putData = [
            'name' => 'Edited Name'
        ];
        $result = $user->toArray();
        $result['name'] = $putData['name'];
        $res = $this->putJson("api/users/$user->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $user = factory(User::class)->create();
        $this->delete("api/users/$user->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
