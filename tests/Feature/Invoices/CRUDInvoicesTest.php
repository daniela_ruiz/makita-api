<?php

namespace Tests\Feature\Invoices;

use App\Category;
use App\Manufacturer;
use App\Product;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class CRUDInvoicesTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $postData = [
            'number' => '73513537',
            'date' => Carbon::now()->toDateString(),
        ];
        $result = [
            'id' => 1,
            'number' => $postData['number'],
            'date' => $postData['date'],
        ];
        $res = $this->postJson("api/invoices", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $invoice = factory(Invoice::class)->create();
        $res = $this->get("api/invoices/$invoice->id");
        $this->assertApiResponse($invoice->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $invoices = factory(Invoice::class, 5)->create();

        $res = $this->get("api/invoices?filter=i");

        $this->assertPaginatedResponse($invoices, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $invoice = factory(Invoice::class)->create();
        $putData = [
            'number' => '123456789'
        ];
        $result = $invoice->toArray();
        $result['number'] = $putData['number'];
        $res = $this->putJson("api/invoices/$invoice->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $invoice = factory(Invoice::class)->create();
        $this->delete("api/invoices/$invoice->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
