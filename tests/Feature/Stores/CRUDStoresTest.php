<?php

namespace Tests\Feature\Stores;

use App\Category;
use App\Manufacturer;
use App\Product;
use App\Role;
use App\Store;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class CRUDStoresTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $postData = [
            'lat' => '77.147489',
            'lng' => '86.211205',
            'title' => 'Company A'
        ];
        $result = array_merge($postData, ['id' => Store::all()->count() + 1]);
        $res = $this->postJson("api/stores", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $store = factory(Store::class)->create();

        $res = $this->get("api/stores/$store->id");

        $this->assertApiResponse($store->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $stores = Store::all();

        $res = $this->get("api/stores");

        $this->assertApiResponse($stores->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $store = factory(Store::class)->create();
        $putData = [
            'title' => 'Edited Title'
        ];
        $result = $store->toArray();
        $result['title'] = $putData['title'];
        $res = $this->putJson("api/stores/$store->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $store = factory(Store::class)->create();
        $this->delete("api/stores/$store->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
