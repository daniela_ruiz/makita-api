<?php

namespace Tests\Feature\Categories;

use App\Category;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class CRUDCategoriesTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $postData = [
            'name' => 'Tools'
        ];
        $result = [
            'id' => 1,
            'name' => $postData['name']
        ];
        $res = $this->postJson("api/categories", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $category = factory(Category::class)->create();
        $res = $this->get("api/categories/$category->id");
        $this->assertApiResponse($category->toArray(), $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $categories = factory(Category::class, 5)->create();
        $res = $this->get("api/categories?filter=ne");
        $this->assertPaginatedResponse($categories, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $category = factory(Category::class)->create();
        $putData = [
            'name' => 'Edit'
        ];
        $result = [
            'id' => 1,
            'name' => $putData['name']
        ];
        $res = $this->putJson("api/categories/$category->id", $putData);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $category = factory(Category::class)->create();
        $this->delete("api/categories/$category->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
