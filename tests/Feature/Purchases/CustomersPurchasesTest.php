<?php

namespace Tests\Feature\Purchases;

use App\Category;
use App\Customer;
use App\Product;
use App\Purchase;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class CustomersPurchasesTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $user1 = factory(User::class)->create();
        $user1->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user1->roles()->attach(Role::findByName('customer'));

        $user2 = factory(User::class)->create();
        $user2->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user2->roles()->attach(Role::findByName('customer'));

        $products = factory(Product::class, 3)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });
        $users = [$user1, $user2];
        $purchases = [];
        for ($i = 0; $i < 2; $i++) {
            for ($j = 0; $j < 2; $j++) {
                $purchase = factory(Purchase::class)->create(['customer_id' => $users[$i]->profile->id,
                    'points' => $products->sum('points')]);
                $purchase->products()->attach($products->pluck('id'));
                unset($purchase['customer_id']);
                if ($i === 0) {
                    $purchases[] = $purchase;
                }
            }
        }
        $res = $this->get("api/customers/$user1->id/purchases?filter=i");
        $this->assertPaginatedResponse($purchases, $res);
    }
}
