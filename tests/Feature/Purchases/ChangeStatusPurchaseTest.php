<?php

namespace Tests\Feature\Purchases;

use App\Category;
use App\Customer;
use App\Product;
use App\Purchase;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class ChangeStatusPurchaseTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdateStatus()
    {
        $user = factory(User::class)->create();
        $user->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user->roles()->attach(Role::findByName('customer'));

        $products = factory(Product::class, 2)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });

        $purchase = factory(Purchase::class)->create(['customer_id' => $user->profile->id,
            'points' => $products->sum('points')]);
        $purchase->products()->attach($products->pluck('id'));

        $putData = [
            'status' => 'valida',
        ];

        $result = $purchase->toArray();
        $result['status'] = $putData['status'];
        unset($result['customer_id']);

        $res = $this->putJson("api/purchases/$purchase->id/verify", $putData);
        $this->assertApiResponse($result, $res);
        $user = $purchase->customer;
        $this->assertEquals($user->points, $products->sum('points'));
    }
}
