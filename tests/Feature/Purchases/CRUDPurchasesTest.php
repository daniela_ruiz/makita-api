<?php

namespace Tests\Feature\Purchases;

use App\Category;
use App\Customer;
use App\Product;
use App\Purchase;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class CRUDPurchasesTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $user = factory(User::class)->create();
        $user->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user->roles()->attach(Role::findByName('customer'));
        Passport::actingAs(
            $user
        );

        $products = factory(Product::class, 2)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });

        $productsData = $products->map(function ($item, $key) {
            return [
                'id' => $item->id,
                'serial' => $this->faker->sha1
            ];
        });

        $postData = [
            'invoice_number' => $this->faker->ean8,
            'invoice_date' => Carbon::now()->toDateString(),
            'products' => $productsData
        ];

        $result = [
            'id' => 1,
            'invoice_number' => $postData['invoice_number'],
            'invoice_date' => $postData['invoice_date'],
            'products' => $products->toArray()
        ];

        $res = $this->postJson("api/purchases", $postData);
        $this->assertApiResponse($result, $res, 201);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $user = factory(User::class)->create();
        $user->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user->roles()->attach(Role::findByName('customer'));
        Passport::actingAs(
            $user
        );
        $products = factory(Product::class, 2)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });
        $purchase = factory(Purchase::class)->create(['customer_id' => $user->profile->id]);
        $purchase->products()->attach($products->pluck('id'));

        $res = $this->get("api/purchases/$purchase->id");
        $result = $purchase->toArray();
        unset($result['customer_id']);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $user = factory(User::class)->create();
        $user->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user->roles()->attach(Role::findByName('customer'));
        Passport::actingAs(
            $user
        );
        $products = factory(Product::class, 2)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });
        $purchases = [];
        for ($i = 0; $i < 3; $i++) {
            $purchase = factory(Purchase::class)->create(['customer_id' => $user->profile->id]);
            $purchase->products()->attach($products->pluck('id'));
            unset($purchase['customer_id']);
            $purchases[] = $purchase;
        }
        $res = $this->get("api/purchases?filter=i");

        $this->assertPaginatedResponse($purchases, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $user->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user->roles()->attach(Role::findByName('customer'));

        $products = factory(Product::class, 2)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });
        $purchase = factory(Purchase::class)->create(['customer_id' => $user->profile->id]);
        $purchase->products()->attach($products->pluck('id'));

        $putData = [
            'invoice_number' => '123456789'
        ];

        $result = $purchase->toArray();
        unset($result['customer_id']);
        $result['invoice_number'] = $putData['invoice_number'];

        $res = $this->putJson("api/purchases/$purchase->id", $putData);

        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $user = factory(User::class)->create();
        $user->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user->roles()->attach(Role::findByName('customer'));
        $products = factory(Product::class, 2)->create()->each(function ($p) {
            $p->categories()->save(factory(Category::class)->create());
        });
        $purchase = factory(Purchase::class)->create(['customer_id' => $user->profile->id]);
        $purchase->products()->attach($products->pluck('id'));

        $this->delete("api/purchases/$purchase->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
