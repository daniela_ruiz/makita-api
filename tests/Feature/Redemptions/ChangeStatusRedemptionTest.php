<?php

namespace Tests\Feature\Redemptions;

use App\Category;
use App\Customer;
use App\Http\Resources\Redemptions;
use App\Product;
use App\Purchase;
use App\Redemption;
use App\Benefit;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\ApiTestTrait;

class ChangeStatusRedemptionTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdateStatus()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make(['points' => 1000])->getAttributes());
        $reward = factory(Benefit::class)->create(['points' => 1000]);

        $redemption = new Redemption();
        $redemption->reward_id = $reward->id;
        $redemption->customer_id = $customer->profile->id;
        $redemption->save();

        $res = $this->putJson("api/redemptions/$redemption->id/approve");
        $result = $redemption->toArray();
        $result['customer']['id'] = $customer->id;
        $result['status'] = Redemption::getEnum('status')['p'];
        unset($result['reward_id']);
        unset($result['customer_id']);
        $this->assertApiResponse($result, $res);
    }
}
