<?php

namespace Tests\Feature\Redemptions;

use App\Customer;
use App\Redemption;
use App\Benefit;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class CRUDRedemptionsTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make(['points' => 1000])->getAttributes());
        $customer->roles()->attach(Role::findByName('customer'));
        Passport::actingAs(
            $customer
        );

        $reward = factory(Benefit::class)->create(['points' => 500]);

        $res = $this->postJson("api/redemptions", ['reward_id' => $reward->id]);

        $result = [
            'id' => 1,
            'reward' => [
                'id' => $reward->id
            ]
        ];
        $this->assertApiResponse($result, $res, 201);
        $this->assertEquals(500, $customer->profile->points);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRead()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make(['points' => 1000])->getAttributes());
        $reward = factory(Benefit::class)->create(['points' => 1000]);

        $redemption = new Redemption();
        $redemption->reward_id = $reward->id;
        $redemption->customer_id = $customer->profile->id;
        $redemption->save();

        $res = $this->get("api/redemptions/$redemption->id");
        $result = $redemption->toArray();
        $result['customer']['id'] = $customer->id;
        unset($result['reward_id']);
        unset($result['customer_id']);
        $this->assertApiResponse($result, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $results = [];
        for ($i=0; $i<5; $i++) {
            $customer = factory(User::class)->create();
            $customer->profile()->create(factory(Customer::class)->make(['points' => 1000])->getAttributes());
            $reward = factory(Benefit::class)->create(['points' => 1000]);

            $redemption = new Redemption();
            $redemption->reward_id = $reward->id;
            $redemption->customer_id = $customer->profile->id;
            $redemption->save();
            unset($redemption->customer_id);
            unset($redemption->reward_id);
            $results[] = $redemption;
        }
        $res = $this->get("api/redemptions?filter=i");
        $this->assertPaginatedResponse($results, $res);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make(['points' => 1000])->getAttributes());
        $reward1 = factory(Benefit::class)->create(['points' => 500]);
        $reward2 = factory(Benefit::class)->create(['points' => 1000]);

        $redemption = new Redemption();
        $redemption->reward_id = $reward1->id;
        $redemption->customer_id = $customer->profile->id;
        $redemption->points = $reward1->points;
        $redemption->save();
        $profile = $customer->profile;
        $profile->points -= $redemption->points;
        $profile->save();

        $res = $this->putJson("api/redemptions/$redemption->id", ['reward_id' => $reward2->id]);
        $result = $redemption->toArray();
        $result['customer']['id'] = $customer->id;
        $result['points'] = 1000;
        unset($result['reward_id']);
        unset($result['customer_id']);

        $result['reward'] = [
            'id' => $reward2->id
        ];
        $this->assertApiResponse($result, $res);

        $this->assertEquals($redemption->customer->points, 0);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDelete()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make(['points' => 1000])->getAttributes());
        $reward = factory(Benefit::class)->create(['points' => 1000]);

        $redemption = new Redemption();
        $redemption->reward_id = $reward->id;
        $redemption->customer_id = $customer->profile->id;
        $redemption->save();

        $this->delete("api/redemptions/$redemption->id")->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
