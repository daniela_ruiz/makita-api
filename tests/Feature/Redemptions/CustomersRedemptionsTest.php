<?php

namespace Tests\Feature\Redemptions;

use App\Category;
use App\Customer;
use App\Product;
use App\Purchase;
use App\Redemption;
use App\Benefit;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class CustomersRedemptionsTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReadAll()
    {
        $user1 = factory(User::class)->create();
        $user1->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user1->roles()->attach(Role::findByName('customer'));

        $user2 = factory(User::class)->create();
        $user2->profile()->create(factory(Customer::class)->make()->getAttributes());
        $user2->roles()->attach(Role::findByName('customer'));

        $users = [$user1, $user2];
        $redemptions = [];
        for ($i=0; $i<2; $i++) {
            for ($j = 0; $j < 3; $j++) {
                $reward = factory(Benefit::class)->create(['points' => 1000]);

                $redemption = new Redemption();
                $redemption->reward_id = $reward->id;
                $redemption->customer_id = $users[$i]->profile->id;
                $redemption->save();
                unset($redemption->customer_id);
                unset($redemption->reward_id);
                if ($i === 0) {
                    $redemptions[] = $redemption;
                }
            }
        }
        $res = $this->get("api/customers/$user1->id/rewards?filter=i");
        $this->assertPaginatedResponse($redemptions, $res);
    }
}
