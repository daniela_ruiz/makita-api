<?php

namespace Tests\Feature\Redemptions;

use App\Customer;
use App\Redemption;
use App\Benefit;
use App\Role;
use App\User;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\ApiTestTrait;

class FailRedemptionsTest extends TestCase
{
    use ApiTestTrait;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCantCreate()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make(['points' => 90])->getAttributes());
        $customer->roles()->attach(Role::findByName('customer'));
        Passport::actingAs(
            $customer
        );

        $reward = factory(Benefit::class)->create(['points' => 100]);

        $this->postJson("api/redemptions", ['reward_id' => $reward->id])->assertStatus(422);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCantUpdate()
    {
        $customer = factory(User::class)->create();
        $customer->profile()->create(factory(Customer::class)->make(['points' => 600])->getAttributes());
        $reward1 = factory(Benefit::class)->create(['points' => 500]);
        $reward2 = factory(Benefit::class)->create(['points' => 700]);

        $redemption = new Redemption();
        $redemption->reward_id = $reward1->id;
        $redemption->customer_id = $customer->profile->id;
        $redemption->points = $reward1->points;
        $redemption->save();
        $profile = $customer->profile;
        $profile->points = $profile->points -$redemption->points;
        $profile->save();

        $this->putJson("api/redemptions/$redemption->id", ['reward_id' => $reward2->id])->assertStatus(422);
        $this->assertEquals(100, $redemption->customer->points);
    }
}
