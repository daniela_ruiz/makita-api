<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Role extends Model
{
    use SoftDeletes, ModelQueryTrait;

    protected $fillable = [
        'name',
        'description'
    ];
    private $search_attributes = [
        'name',
        'description'
    ];

    protected $table = 'roles';


    /**
     * The users that belong to the role.
     */

    public function users()
    {
        return $this->belongsToMany('App\User', 'role_user');
    }

    public static function findByName($string)
    {
        return Role::where('name', $string)->first();
    }

    public function getAllRoles()
    {
        return Role::pluck('name')->toArray();
    }

    public function getOpRoles()
    {
        return Role::where([
            ['name', '<>', 'customer'],
        ])->pluck('name')->toArray();
    }

    public function queryRoles(Request $request)
    {
        $query = $this->modelQuery(Role::class, $request, $this->search_attributes);
        return $query;
    }
}
