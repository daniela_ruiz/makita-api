<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Configuration extends Model
{
    use SoftDeletes, ModelQueryTrait;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'configurations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'type', 'description'
    ];

    public $search_attributes = [
        'name', 'value', 'type', 'description'
    ];

    public static function get($name, $default = null)
    {
        $config = Configuration::where('name', $name)->first();
        return $config ? $config->value : $default;
    }

    public function getValueAttribute($value)
    {
        switch ($this->type) {
            case 'string':
                return $value;
                break;
            case 'time':
                return $value;
                break;
            case 'decimal':
                return floatval($value);
                break;
            case 'integer':
                return intval($value);
                break;
            case 'boolean':
                return filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
        }
    }

    public function queryConfig(Request $request)
    {
        $query = $this->modelQuery(Configuration::class, $request, $this->search_attributes);
        return $query;
    }
}
