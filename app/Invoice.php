<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Invoice extends Model
{
    use ModelQueryTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'date',
    ];

    private $search_attributes = [
        'number',
        'date',
    ];

    public function queryInvoices(Request $request)
    {
        $query = $this->modelQuery(Invoice::class, $request, $this->search_attributes);
        return $query;
    }
}
