<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportBenefitCustomer extends ReportModel {
  var $product_id;
  var $total;
  var $workshop_id;
  var $approved;
  var $benefit_id;

  protected $fillable = ["product_id","total","workshop_id","approved", "benefit_id"];

  protected $casts = [
    "approved" => "boolean"
  ];

  protected $hidden = ["product_id", "workshop_id", "benefit_id"];

  static function getReportQuery(Request $request, $defaultSelect = [], $defaultGroupBy = []) {
    $query = DB::table("benefit_customer as bc");

    ReportModel::processDatesInQuery($query, $request, "bc.created_at");

    $select = array_merge([DB::raw("count(*) as total")], $defaultSelect);
    $groupBy = $defaultGroupBy;

    if ($request->has("workshop")) {
      $select[] = "bc.workshop_id";
      $groupBy[] = "bc.workshop_id";
      $query->where("bc.workshop_id",$request->input("workshop"));
    }

    if ($request->has("customer") && $request->input("customer")) {
      $query->where("bc.customer_id",$request->input("customer"));
      $select[] = "bc.customer_id";
      $groupBy[] = "bc.customer_id";
    }

    if ($request->has("product") && $request->input("product")) {
      $query->where("product_id",$request->input("product"));
      $select[] = "product_id";
      $groupBy[] = "product_id";
    }

    if ($request->has("benefit")) {
      $select[] = "bc.benefit_id";
      $groupBy[] = "bc.benefit_id";
      $query->where("bc.benefit_id",$request->input("benefit"));
    }

    if ($request->has("approved")) {
      $select[] = "bc.approved";
      $groupBy[] = "bc.approved";
      $query->where("bc.approved",$request->input("approved"));
    }

    $query->select($select)
      ->join("purchases as p", "p.id", "=", "bc.purchase_id")
      ->groupBy($groupBy);

    return $query;
  }


  function product(){
    return $this->hasOne("App\Product", "id", "product_id");
  }

  function workshop() {
    return $this->hasOne("App\Workshop", "id", "workshop_id");
  }

  function benefit() {
    return $this->hasOne("App\Benefit", "id", "benefit_id");
  }
}