<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportPurchase extends ReportModel {
  var $product_id;
  var $total;
  var $store_id;

  protected $fillable = ["product_id","total","store_id"];

  protected $hidden = ["product_id", "store_id"];

  static function getReportQuery(Request $request, $defaultSelect = [], $defaultGroupBy = []){
    $groupBy = ["product_id"];
    $select = [DB::raw("count(*) as total"), "product_id"];
    $query = DB::table("purchases");

    ReportModel::processDatesInQuery($query, $request);

    if ($request->has("store") && $request->input("store")) {
      $query->where("store_id",$request->input("store"));
      $select[] = "store_id";
      $groupBy[] = "store_id";
    }

    if ($request->has("customer") && $request->input("customer")) {
      $query->where("customer_id",$request->input("customer"));
      $select[] = "customer_id";
      $groupBy[] = "customer_id";
    }

    if ($request->has("product") && $request->input("product")) {
      $query->where("product_id",$request->input("product"));
    }

    $query->select($select)->groupBy($groupBy);

    return $query;
  }

  function product(){
    return $this->hasOne("App\Product", "id", "product_id");
  }

  function store(){
    return $this->hasOne("App\Store", "id", "store_id");
  }
}