<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class Store extends Model
{
    use ModelQueryTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lat', 'lng', 'title'
    ];
    public $search_attributes = ['title'];

    public function queryStores(Request $request)
    {
        $query = $this->modelQuery(Store::class, $request, $this->search_attributes);

        return $query;
    }
}
