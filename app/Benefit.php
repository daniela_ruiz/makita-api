<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Benefit extends Model
{
    use ModelQueryTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'level_id',
        'points'
    ];

    public $search_attributes = [
        'name',
        'description',
    ];

    public $with = ['level'];

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function claimed()
    {
        return $this->hasMany('App\CustomerBenefit');
    }

    public function scopeByLevel($query, $points)
    {
        $level = Level::calculateLevel($points);

        return $query->where('level_id', !is_null($level) ? $level->id : -1);
    }

    public function queryBenefits(Request $request)
    {
        $query = $this->modelQuery(Benefit::class, $request, $this->search_attributes);
        if ($request->has('level')) {
            $query->byLevel($request->get('level'));
//            if ($request->has('customer')) {
//                $customer = $request->get('customer');
//                $query->whereDoesntHave('claimed', function ($q) use ($customer) {
//                    $q->where('customer_id', '=', $customer);
//                });
//            }
        }
        return $query;
    }
}
