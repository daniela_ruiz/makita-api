<?php

namespace App\Policies;

use App\CustomerBenefit;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerBenefitPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the purchase.
     *
     * @param  \App\User $user
     * @param CustomerBenefit $customerBenefit
     * @return mixed
     */
    public function view(User $user, CustomerBenefit $customerBenefit)
    {
        if ($user->isStaff()) {
            return true;
        } elseif ($user->hasRole('customer')) {
            if (isset($user->profile)) {
                return $user->profile->id === $customerBenefit->customer->id;
            }
        }
    }

    /**
     * Determine whether the user can create purchases.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the purchase.
     *
     * @param  \App\User $user
     * @param CustomerBenefit $customerBenefit
     * @return mixed
     * @internal param Purchase $purchase
     */
    public function update(User $user, CustomerBenefit $customerBenefit)
    {
        if ($user->isStaff()) {
            return true;
        } elseif ($user->hasRole('customer')) {
            if (isset($user->profile)) {
                return $user->profile->id === $customerBenefit->customer->id;
            }
        }
        return false;
    }

    /**
     * Determine whether the user can delete the purchase.
     *
     * @param  \App\User $user
     * @param CustomerBenefit $customerBenefit
     * @return mixed
     */
    public function delete(User $user, CustomerBenefit $customerBenefit)
    {
        //
    }

    /**
     * Determine whether the user can restore the purchase.
     *
     * @param  \App\User $user
     * @param CustomerBenefit $customerBenefit
     * @return mixed
     */
    public function restore(User $user, CustomerBenefit $customerBenefit)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the purchase.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAll(User $user)
    {
        if ($user->isStaff()) {
            return true;
        } elseif ($user->hasRole('customer')) {
            return !is_null($user->profile);
        }
        return false;
    }
}
