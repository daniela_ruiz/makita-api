<?php

namespace App\Policies;

use App\User;
use App\CreditNote;
use Illuminate\Auth\Access\HandlesAuthorization;

class CreditNotePolicy {

  use HandlesAuthorization;

  public function view(User $user, CreditNote $creditNote) {
    if ($user->hasRole("admin")) {
      return TRUE;
    }
    $creditNoteWorkshop = $creditNote->customer_benefit->workshop->id;
    return $this->viewAll($user) && $user->roles->pluck("pivot.workshop_id")->contains($creditNoteWorkshop);
  }

  public function viewAll(User $user) {
    return $user->hasRole('admin') || $user->hasRole('operator') || $user->hasRole('accountant');
  }

  public function create(User $user) {
    return FALSE;
  }

  public function delete(User $user, CreditNote $creditNote) {
    return $this->view($user, $creditNote);
  }

  public function cancel(User $user, CreditNote $creditNote) {
    return $this->view($user, $creditNote);
  }

  public function process(User $user, CreditNote $creditNote) {
    return $this->view($user, $creditNote);
  }
}
