<?php

namespace App\Policies;

use App\User;
use App\Workshop;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorkshopPolicy
{
    use HandlesAuthorization;

    public function addUser(User $user) {
      return $user->hasRole('admin') || $user->hasRole('operator');
    }
}
