<?php
/**
 * Created by PhpStorm.
 * User: apokdev
 * Date: 8/29/18
 * Time: 5:58 AM
 */

namespace App\Traits;

use App\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

trait ModelQueryTrait
{
    /**
     * @param Request $request
     * @return array
     */
    protected function getSortQuery(Request $request)
    {
        $sort = ['created_at', 'DESC'];

        if ($request->has('sort') && !is_null($request->input('sort'))) {
            $sort = explode('|', urldecode($request->input('sort')), 2);
        }
        switch ($sort[0]) {
            case 'category':
                $sort[0] = 'category_name';
                break;
            case 'customer':
                $sort[0] = 'username';
                break;
            case 'product':
                $sort[0] = 'product_name';
                break;
            case 'level':
                $sort[0] = 'level_points';
                break;
            case 'benefit':
                $sort[0] = 'benefit_name';
                break;
            case 'workshop':
                $sort[0] = 'workshop_name';
                break;
        }
        return $sort;
    }

    public function modelQuery(
        $model,
        Request $request = null,
        $attributes = []
    ) {
        $filter = $request->query('filter', null);
        $from = $request->query('from', null);
        $to = $request->query('to', null);
        $orderBy = $this->getSortQuery($request);
        $query = $model::where(
            function ($query) use ($filter, $attributes) {
                if ($filter) {
                    $query->where(function ($query) use ($filter, $attributes) {
                        foreach ($attributes as $attribute) {
                            $query->orWhere($attribute, 'like', "%$filter%");
                        }
                    });
                }
            }
        );
        if (!empty($from) && !empty($to)) {
            $dateFrom = Carbon::createFromFormat('Y-m-d', $from)->hour(00)->minute(00)->second(00);
            $dateTo = Carbon::createFromFormat('Y-m-d', $to)->hour(23)->minute(59)->second(59);

            $query->whereBetween('created_at', [$dateFrom, $dateTo]);
        }
        $query->orderBy($orderBy[0], $orderBy[1]);
        return $query;
    }
}
