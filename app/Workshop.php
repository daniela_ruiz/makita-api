<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class Workshop extends Model
{
    use ModelQueryTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lat', 'lng', 'title'
    ];

    public $search_attributes = ['title'];

    protected $hidden = ["deleted_at"];

    public function queryWorkshops(Request $request)
    {
        $query = $this->modelQuery(Workshop::class, $request, $this->search_attributes);

        return $query;
    }
}
