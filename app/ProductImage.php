<?php

namespace App;

use App\Traits\Enums;
use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductImage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'products_images';

    protected $fillable = [
        'image',
    ];

    /**
     * The product that belongs to the image.
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function getImageAttribute($value)
    {
        return !is_null($value) ? env('APP_ENV') == 'production' ?
            secure_url(Storage::url($value)) : url(Storage::url($value)) : null;
    }

}
