<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CreditNote extends Model {

    use SoftDeletes;

    protected $fillable = [
        "benefit_customer_id",
        "status",
        "price",
    ];

    protected $casts = [
        "price" => "float",
    ];

    protected $with = [
        "customer_benefit.benefit",
        "customer_benefit.customer",
        "customer_benefit.purchase.product"
    ];

    function customer_benefit() {
        return $this->hasOne("App\CustomerBenefit", "id", "benefit_customer_id");
    }

    function scopeByWorkshop($query, $workshop_id) {
        return $query->join("benefit_customer", "benefit_customer_id", "=", "benefit_customer.id")->where(
            "benefit_customer.workshop_id", $workshop_id
        );
    }

    function scopeByStatus($query, $status) {
        return $query->where("status", $status);
    }

    function scopePending($query) {
        return $query->where("status", "pending");
    }

    function scopeByBenefit($query, $benefit) {
        return $query->join("benefit_customer as bc", "bc.id", "=", "credit_notes.benefit_customer_id")
            ->where("bc.benefit_id", $benefit);
    }

    static function queryCreditNotes(Request $request) {
        $query = CreditNote::select("credit_notes.*")
            ->orderBy("credit_notes.created_at", "desc");
        if ($request->has("workshop")) {
            $query->byWorkshop($request->workshop);
        }
        if ($request->has("status")) {
            $query->byStatus($request->status);
        }
        if ($request->has("benefit")) {
            $query->byBenefit($request->benefit);
        }
        if ($request->has("from") && $request->has("to")) {
            $dateFrom = Carbon::createFromFormat('Y-m-d', $request->from)->hour(00)->minute(00)->second(00);
            $dateTo = Carbon::createFromFormat('Y-m-d', $request->to)->hour(23)->minute(59)->second(59);
            $query->whereBetween('credit_notes.created_at', [$dateFrom, $dateTo]);
        }
        return $query;
    }
}
