<?php

namespace App\Listeners;

use App\CreditNote;
use App\Events\BenefitApprovedEvent;

class BenefitApprovedEventListener {

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct() { }

  /**
   * Handle the event.
   *
   * @param  BenefitApprovedEvent $event
   *
   * @return void
   */
  public function handle(BenefitApprovedEvent $event) {
    $customberBenefit = $event->customerBenefit;
    CreditNote::create(
      [
        "benefit_customer_id" => $customberBenefit->id,
        "price"               => $event->price,
        "status"              => "pending"
      ]
    );
  }
}
