<?php

namespace App\Observers;

use App\CustomerBenefit;
use Illuminate\Support\Facades\Log;


class CustomerBenefitObserver
{
    /**
     * Handle the customer benefit "created" event.
     *
     * @param  \App\CustomerBenefit  $customerBenefit
     * @return void
     */
    public function created(CustomerBenefit $customerBenefit)
    {
        //
    }

    /**
     * Handle the customer benefit "updated" event.
     *
     * @param  \App\CustomerBenefit  $customerBenefit
     * @return void
     */
    public function updated(CustomerBenefit $customerBenefit)
    {
        if ($customerBenefit->approved && !is_null($customerBenefit->benefit->points)) {
            $customer = $customerBenefit->customer;
            if ($customer->points > 0) {
                $customer->points -= $customerBenefit->benefit->points;
                $customer->level()->associate($customer->getLevel());
                $customer->save();
            }
        }
    }


    /**
     * Handle the customer benefit "deleted" event.
     *
     * @param  \App\CustomerBenefit  $customerBenefit
     * @return void
     */
    public function deleted(CustomerBenefit $customerBenefit)
    {
        //
    }

    /**
     * Handle the customer benefit "restored" event.
     *
     * @param  \App\CustomerBenefit  $customerBenefit
     * @return void
     */
    public function restored(CustomerBenefit $customerBenefit)
    {
        //
    }

    /**
     * Handle the customer benefit "force deleted" event.
     *
     * @param  \App\CustomerBenefit  $customerBenefit
     * @return void
     */
    public function forceDeleted(CustomerBenefit $customerBenefit)
    {
        //
    }
}
