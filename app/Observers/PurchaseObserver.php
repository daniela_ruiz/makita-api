<?php

namespace App\Observers;

use App\Facades\DateString;
use App\Purchase;
use Carbon\Carbon;
use DateInterval;
use Illuminate\Support\Facades\Log;

class PurchaseObserver
{
    public function creating(Purchase $purchase)
    {
        $string_date_product = DateString::string($purchase->product->warranty, $purchase->product->warranty_time);
        $string_date_ext = DateString::string($purchase->extended_warranty ?:0, $purchase->extended_warranty_time);
        $purchase->extended_warranty_date = Carbon::parse($purchase->invoice_date)
            ->add(DateInterval::createFromDateString($string_date_product.' + '.$string_date_ext))->toDateString();
    }
    /**
     * Handle the purchase "created" event.
     *
     * @param  \App\Purchase  $purchase
     * @return void
     */
    public function created(Purchase $purchase)
    {
        //
    }

    /**
     * Handle the purchase "updated" event.
     *
     * @param  \App\Purchase  $purchase
     * @return void
     */
    public function updated(Purchase $purchase)
    {
        $multiplier = 1;
        if ($purchase->isDirty('status')) {
            if ($purchase->status === Purchase::getEnum('status')['v']) {
                if (!empty($purchase->customer)) {
                    if (!empty($purchase->customer->level)) {
                        if (!empty($purchase->customer->level->multiplier)) {
                            $multiplier = $purchase->customer->level->multiplier;
                        }
                    }
                }
                $customer = $purchase->customer;
                $customer->points += ($purchase->points * $multiplier);
                $customer->level()->associate($customer->getLevel());
                $customer->save();
            }
        }
    }
    /**
     * Handle the purchase "saving" event.
     *
     * @param  \App\Purchase  $purchase
     * @return void
     */
    public function saving(Purchase $purchase)
    {

    }

    /**
     * Handle the purchase "saving" event.
     *
     * @param  \App\Purchase  $purchase
     * @return void
     */
    public function saved(Purchase $purchase)
    {

    }

    /**
     * Handle the purchase "deleted" event.
     *
     * @param  \App\Purchase  $purchase
     * @return void
     */
    public function deleted(Purchase $purchase)
    {
        //
    }

    /**
     * Handle the purchase "restored" event.
     *
     * @param  \App\Purchase  $purchase
     * @return void
     */
    public function restored(Purchase $purchase)
    {
        //
    }

    /**
     * Handle the purchase "force deleted" event.
     *
     * @param  \App\Purchase  $purchase
     * @return void
     */
    public function forceDeleted(Purchase $purchase)
    {
        //
    }
}
