<?php

namespace App;

use App\Traits\Enums;
use App\Traits\ModelQueryTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use ModelQueryTrait, Enums, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'points',
        'warranty',
        'warranty_time',
        'sku',
        'service_time',
        'model',
        'description'
    ];

    protected $attributes = [
        'warranty' => 1,
        'warranty_time' => 'A',
        'service_time' => 6,
    ];

    // Define all of the valid options in an array as a protected property that
    //    starts with 'enum' followed by the plural studly cased field name
    protected $enumWarrantyTimes = [
        'd' => 'D',
        'm' => 'M',
        'a' => 'A',
    ];

    public $search_attributes = [
        'name',
        'points',
        'sku',
        'model',
        'description'
    ];

    public function getImageAttribute($value)
    {
        if (!is_null($value)) {
            return env('APP_ENV') == 'production' ? secure_url(Storage::url($value)) : url(Storage::url($value));
        } else {
            if ($this->images()->exists()) {
                return $this->images()->first()->image;
            }
        }
        return null;
    }

    /**
     * The categories that belong to the product.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * The segments that belong to the product.
     */
    public function segment()
    {
        return $this->belongsTo('App\Segment');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function scopeByCategory($query, $filter)
    {
        $category = new Category();
        $attributes = $category->search_attributes;
        $query->whereHas('category', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    if ($filter != "") {
                        $query->where($attribute, 'like', "%$filter%");
                    }
                }
            });
        });
        return $query;
    }

    public function scopeBySegment($query, $filter)
    {
        $segment = new Segment();
        $attributes = $segment->search_attributes;
        $query->whereHas('segment', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    $query->where($attribute, 'like', "%$filter%");
                }
            });
        });
        return $query;
    }

    public function queryProducts(Request $request)
    {
        if (!$request->has('sort')) {
            $request->request->add(['sort' => 'name|ASC']);
        }

        $filter = $request->query('filter', null);
        $from = $request->query('from', null);
        $to = $request->query('to', null);
        $orderBy = $this->getSortQuery($request);
        $attributes = $this->search_attributes;
        $query = Product::where(
            function ($query) use ($filter, $attributes) {
                if ($filter) {
                    $query->where(function ($query) use ($filter, $attributes) {
                        foreach ($attributes as $attribute) {
                            $query->orWhere('products.'.$attribute, 'like', "%$filter%");
                        }
                    });
                }
            }
        );
        if (!empty($from) && !empty($to)) {
            $dateFrom = Carbon::createFromFormat('Y-m-d', $from)->hour(00)->minute(00)->second(00);
            $dateTo = Carbon::createFromFormat('Y-m-d', $to)->hour(23)->minute(59)->second(59);

            $query->whereBetween('created_at', [$dateFrom, $dateTo]);
        }

        if ($request->has('category')) {
            if ($request->get('category') != null) {
                $filter = $request->get('category');
                $query->byCategory($filter);
            }
        }

        $query->join('categories', 'categories.id', 'products.category_id')
            ->select('products.*', 'categories.name as category_name')
            ->orderBy($orderBy[0], $orderBy[1]);

        return $query;
    }
}
