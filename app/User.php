<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {

  use Notifiable, HasApiTokens, ModelQueryTrait, SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable
    = [
      'name',
      'email',
      'password',
      'email_confirmed',
      'confirm_code',
    ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden
    = [
      'password',
      'remember_token',
    ];

  protected $with
    = [
      'roles',
    ];

  public $search_attributes
    = [
      'name',
      'email',
    ];

  /**
   * Set the user's email
   *
   * @param  string $value
   *
   * @return void
   */
  public function setEmailAttribute($value) {
    $this->attributes['email'] = strtolower($value);
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function roles() {
    return $this->belongsToMany('App\Role', 'role_user')->withPivot(["workshop_id"]);
  }

  public function profile() {
    return $this->hasOne('App\Customer');
  }

  /**
   * Scope a query to only include users of a given type.
   *
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function scopeStaff($query) {
    return $query->whereHas(
      'roles', function ($q) {
      $q->where('name', '<>', 'customer');
    }
    );
  }

  public function hasRole($role) {
    return $this->roles->contains('name', $role);
  }

  public function isStaff() {
    return $this->hasRole('admin') || $this->hasRole('operator') || $this->hasRole("accountant");
  }

  public function scopeByCustomerFilter($query, $filter, $attributes) {
    $query->orWhereHas(
      'profile', function ($q) use ($filter, $attributes) {
      $q->where(
        function ($query) use ($filter, $attributes) {
          foreach ($attributes as $attribute) {
            $query->orWhere($attribute, 'like', "%$filter%");
          }
        }
      );
    }
    );
  }

  public function queryUsers(Request $request) {
    $query = $this->modelQuery(User::class, $request, $this->search_attributes)->select("users.*");
    $query->staff();
    return $query;
  }

  public function workshop(){
    if ($this->hasRole('operator')){
      $roleUser = $this->roles()->withPivot("workshop_id")
        ->where("name","operator")->first();
      if (!is_null($roleUser)) {
        return $roleUser->pivot->workshop_id;
      }
      return null;
    }
    return null;
  }
}
