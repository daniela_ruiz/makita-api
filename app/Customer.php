<?php

namespace App;

use App\Facades\FirebaseNotification;
use App\Traits\ModelQueryTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    use ModelQueryTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'birth_date',
        'gender',
        'subscription',
        'firebase_uid',
        'level',
        'profession'
    ];

    protected $casts = [
        'points' => 'integer',
        'subscription' => 'boolean'
    ];

    protected $with = [
        'user',
        'level'
    ];
    protected $search_attributes = [
        'user' => [
            'name',
            'email'
        ],
        'customer' => [
            'birth_date',
            'gender',
        ]
    ];

    /**
     * Get the user details for the customer.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function benefits()
    {
        return $this->hasMany('App\CustomerBenefit');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchases()
    {
        return $this->hasMany('App\Purchase');
    }
    public function devices()
    {
        return $this->hasMany("App\Device");
    }

    public function getLevel()
    {
        return Level::calculateLevel($this->points);
    }

    public function notify($notification)
    {
        $devices = $this->devices()->where('active', '=', true)->pluck('device_id')->all();

        if (count($devices)) {
            FirebaseNotification::sendNotification($devices, [
                "id" => $notification->id,
                "type" => $notification->type,
                "title" => $notification->title,
                "message" => $notification->description,
                "image" => $notification->image,
                "date" => $notification->date
            ]);
        }
    }

    public function queryCustomers(Request $request)
    {
//        $query = $this->modelQuery(User::class, $request, $this->search_attributes['user']);
//        $query->has('profile');
//        if ($request->has('filter')) {
//            $filter = $request->get('filter');
//            $attributes = $this->search_attributes['customer'];
//            $query->byCustomerFilter($filter, $attributes);
//        }
//        $query->whereHas('roles', function ($query) {
//            $query->where('name', 'customer');
//        });

        $filter = $request->query('filter', null);
        $from = $request->query('from', null);
        $to = $request->query('to', null);
        $orderBy = $this->getSortQuery($request);
        $attributes = $this->search_attributes['user'];

        $query = User::where(
            function ($query) use ($filter, $attributes) {
                if ($filter) {
                    $query->where(function ($query) use ($filter, $attributes) {
                        foreach ($attributes as $attribute) {
                            $query->orWhere($attribute, 'like', "%$filter%");
                        }
                    });
                }
            }
        );

        $query->whereHas('roles', function ($query) {
            $query->where('name', 'customer');
        });

        if (!empty($from) && !empty($to)) {
            $dateFrom = Carbon::createFromFormat('Y-m-d', $from)->hour(00)->minute(00)->second(00);
            $dateTo = Carbon::createFromFormat('Y-m-d', $to)->hour(23)->minute(59)->second(59);

            $query->whereBetween('created_at', [$dateFrom, $dateTo]);
        }

        if ($request->has('customer')) {
            $query->where('customer_id', $request->get('customer'));
        }

        $query->join(DB::raw("(SELECT customers.user_id, levels.points FROM customers JOIN levels ON customers.level_id = levels.id) as t"), 't.user_id', '=', 'users.id')
            ->select('users.*', 't.points as level_points')
            ->orderBy($orderBy[0], $orderBy[1]);

        return $query;
    }
}
