<?php

namespace App\Http\Resources;

use App\Facades\DateString;
use App\Product;
use Carbon\Carbon;
use DateInterval;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PurchaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->load('product');
        $this->load('store');
        $this->load('customer');

        return [
            'id' => $this->id,
            'invoice_number' => $this->invoice_number,
            'invoice_date' => $this->invoice_date,
            'status' => $this->status,
            'points' => $this->points,
            'next_service' => $this->next_service,
            'serial' => $this->serial,
            'invoice' => $this->getAttribute('invoice'),
            'product' => new ProductResource($this->whenLoaded('product')),
            'store' => $this->whenLoaded('store'),
            'customer' => $this->when(Auth::user()->isStaff(), $this->customer),
            'extended_warranty_date' => $this->extended_warranty_date,
            'created_at' =>  (string) $this->created_at,
            'updated_at' => (string) $this->updated_at
        ];
    }
}
