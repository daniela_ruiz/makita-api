<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class CustomerBenefitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->load('customer');
        $this->load('workshop');
        $this->load('benefit');
        $this->load('purchase.product');
        return [
            'id' => $this->id,
            'approved' => $this->approved,
            'price' => $this->price,
            'purchase' => $this->purchase,
            'customer' => $this->when(Auth::user()->isStaff(), $this->customer),
            'workshop' => $this->workshop,
            'benefit' => $this->benefit,
            'created_at' =>  (string) $this->created_at,
            'updated_at' => (string) $this->updated_at
        ];
    }
}
