<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->load('profile');
        $this->load('roles');
        $profile = $this->profile;
        $dt1 = Carbon::createFromFormat('Y-m-d H:i:s', (string) $this->updated_at);
        $dt2 = (isset($profile) ?
            Carbon::createFromFormat('Y-m-d H:i:s', (string) $profile->updated_at) : Carbon::minValue());
        return [
            'id' => $this->hasRole('customer') ? $this->profile->id : $this->id,
            'name' => $this->name,
            'email' => $this->email,
            $this->mergeWhen(
                $this->profile()->exists(),
                $this->profile ? $this->profile->attributesToArray() : null
            ),
            'roles' => $this->roles,
            'created_at' =>  (string) $this->created_at,
            'updated_at' => $dt1->max($dt2)->toDateTimeString()
        ];
    }
}
