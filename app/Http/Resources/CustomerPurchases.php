<?php

namespace App\Http\Resources;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CustomerPurchases extends ResourceCollection
{
    use SoftDeletes;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PurchaseResource::collection($this->collection)
        ];
    }
    public function with($request)
    {
        $total = $this->collection->sum('points');

        return [
            'report' => [
                'total_points' => $total
            ],
        ];
    }
}
