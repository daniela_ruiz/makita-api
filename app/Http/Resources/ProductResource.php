<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->attributes([
                'id',
                'name',
                'sku',
                'points',
                'warranty',
                'warranty_time',
                'service_time',
                'model',
                'description',
                'created_at',
                'updated_at']),
            'image' => $this->image,
            'category' => $this->category,
            'images' => $this->images()->get()
          //  'segment' => $this->segment,
        ];
    }
}
