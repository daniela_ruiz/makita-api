<?php

namespace App\Http\Requests\Configuration;

use App\Http\Requests\FormRequest;

class UpdateConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = isset($this->type) ? $this->type : $this->route('configuration')->type;

        switch ($type) {
            case 'decimal':
                $e = 'regex:/^\d{1,8}(\.\d{1,})?$/';
                break;
            case 'boolean':
                $e = 'boolean';
                break;
            case 'time':
                $e = 'in:D,M,A';
                break;
            case 'integer':
                $e = 'integer';
                break;
            case 'string':
                $e = 'string';
                break;
            default:
                $e = '';
        }

        return [
            'type' => 'in:string,integer,decimal,boolean,time',
            'value' => $e
        ];
    }
}
