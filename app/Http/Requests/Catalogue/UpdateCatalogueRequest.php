<?php

namespace App\Http\Requests\Catalogue;

use App\Http\Requests\FormRequest;
use App\Notification;
use Illuminate\Validation\Rule;

class UpdateCatalogueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "string",
            "description" => "string",
            "image" => "file|mimes:jpeg,bmp,png,gif|max:2000",
            "file" => "file|mimes:pdf|max:2000",
            "active" => "boolean"
        ];
    }
}
