<?php

namespace App\Http\Requests\Catalogue;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class CreateCatalogueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "description" => "string",
            "image" => "required|file|mimes:jpeg,bmp,png,gif",
            "file" => "required|file|mimes:pdf",
            "active" => "boolean"
        ];
    }
}
