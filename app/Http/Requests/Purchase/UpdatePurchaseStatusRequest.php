<?php

namespace App\Http\Requests\Purchase;

use App\Purchase;
use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePurchaseStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "status" => [
                "required",
                Rule::in(Purchase::getEnum('status'))
            ]
        ];
    }
}
