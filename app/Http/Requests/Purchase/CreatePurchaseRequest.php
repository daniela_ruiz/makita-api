<?php

namespace App\Http\Requests\Purchase;

use App\Http\Requests\FormRequest;

class CreatePurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (is_null($this->user()->profile)) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "invoice_number" => 'required|string',
            "invoice_date" => 'required|date',
            'product_id' => 'required|exists:products,id',
            'store_id' => 'required|exists:stores,id',
            'serial' => 'required|string',
            'invoice' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
    }
}
