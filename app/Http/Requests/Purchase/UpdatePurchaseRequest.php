<?php

namespace App\Http\Requests\Purchase;

use App\Http\Requests\FormRequest;

class UpdatePurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "invoice_number" => 'string',
            "invoice_date" => 'date',
            'product_id' => 'exists:products,id',
            'store_id' => 'exists:stores,id',
            'serial' => 'string',
        ];
    }
}
