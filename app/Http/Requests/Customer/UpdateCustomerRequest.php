<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Log;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('customer') ? $this->route('customer')->user: $this->user();
        return [
            'name' => 'string',
            'email' => "string|email|max:255|unique:users,email,$user->id",
            'password' => 'string|min:6|confirmed|nullable',
            'birth_date' => 'date|before:today',
            'gender' => 'string|max:255|in:Masculino,Femenino,masculino,femenino,m,f,M,F,Otro,otro',
            'profession' => 'string'
        ];
    }
}
