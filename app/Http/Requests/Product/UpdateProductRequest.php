<?php

namespace App\Http\Requests\Product;

use App\Product;
use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'points' => 'integer',
            'warranty' => 'integer',
            'warranty_time' => [
                Rule::in(Product::getEnum('warranty_time'))
            ],
            'service_time' => 'integer',
            'model' => 'string',
            'sku' => 'string',
            'category_id' => 'exists:categories,id',
//            'segment_id' => 'exists:segments,id',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
    }
}
