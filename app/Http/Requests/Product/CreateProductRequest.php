<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\FormRequest;
use App\Product;
use Illuminate\Validation\Rule;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'points' => 'required|integer',
            'description' => 'string',
            'warranty' => 'integer',
            'warranty_time' => [
                Rule::in(Product::getEnum('warranty_time'))
            ],
            'service_time' => 'integer',
            'model' => 'required|string',
            'sku' => 'required|string',
            'category_id' => 'required|exists:categories,id',
//            'segment_id' => 'required|exists:segments,id',
            'image1' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'image2' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'image3' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'image4' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'image5' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ];
    }
}
