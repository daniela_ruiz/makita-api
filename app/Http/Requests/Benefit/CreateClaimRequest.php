<?php

namespace App\Http\Requests\Reward;

use App\Http\Requests\FormRequest;

class CreateClaimRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'purchase_id' => 'required|exists:purchases,id',
            'benefit_id' => 'required|exists:benefits,id',
            'workshop_id' => 'required|exists:workshops,id',
        ];
    }
}
