<?php

namespace App\Http\Requests\Notification;

use App\Http\Requests\FormRequest;
use App\Notification;
use Illuminate\Validation\Rule;

class UpdateNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "string",
            "description" => "string",
            "type" => [
                Rule::in(Notification::getEnum('type'))
            ],
            "date" => 'date',
            "image" => "file|mimes:jpeg,bmp,png,gif"
        ];
    }
}
