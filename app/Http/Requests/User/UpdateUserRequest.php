<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user');
        return [
            'name' => 'string|max:255',
            'email' => "string|email|max:255|unique:users,email,$user->id",
            'password' => 'string|min:6|confirmed|nullable',
            'roles.*' => 'exists:roles,id',
        ];
    }
}
