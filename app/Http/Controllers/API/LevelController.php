<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Level\CreateLevelRequest;
use App\Http\Requests\Level\UpdateLevelRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Level;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LevelController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource
     */
    public function index(Request $request)
    {
        $level = new Level();

        return $this->sendPaginatedResponse(
            $request,
            $level->queryLevels($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateLevelRequest|Request $request
     * @return APIResponseResource
     */
    public function store(CreateLevelRequest $request)
    {
        $data = $request->except(['image']);
        $level = new Level();
        $level->fill($data);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('levels');
            $level->image = $path;
        }
        $level->slug = str_slug($level->name, '_');
        $level->save();

        return new APIResponseResource($level);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Level  $level
     * @return APIResponseResource
     */
    public function show(Level $level)
    {
        return new APIResponseResource($level);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateLevelRequest  $request
     * @param  \App\Level  $level
     * @return APIResponseResource
     */
    public function update(UpdateLevelRequest $request, Level $level)
    {
        $data = $request->except(['image']);
        $level->fill($data);
        if ($request->hasFile('image')) {
            Storage::delete($level->getOriginal('image'));
            $path = $request->file('image')->store('levels');
            $level->image = $path;
        }
        if ($request->has('name')) {
            $level->slug = str_slug($request->get('name'), '_');
        }
        $level->save();

        return new APIResponseResource($level);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Level  $level
     * @return LevelController
     */
    public function destroy(Level $level)
    {
        $level->delete();
        return $this->sendDeletedResponse();
    }
}
