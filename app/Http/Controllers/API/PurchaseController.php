<?php

namespace App\Http\Controllers\API;

use App\Configuration;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Purchase\CreatePurchaseRequest;
use App\Http\Requests\Purchase\UpdatePurchaseRequest;
use App\Http\Requests\Purchase\UpdatePurchaseStatusRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Http\Resources\CustomerPurchases;
use App\Http\Resources\PurchaseResource;
use App\Http\Resources\Purchases;
use App\Product;
use App\Purchase;
use App\Store;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PurchaseController extends Controller {

  use IndexControllerTrait, SoftDeletes;

  /**
   * Display a listing of the resource.
   *
   * @param Request $request
   *
   * @return APIResponseResource
   */
  public function index(Request $request) {
      $user = $request->user();
      $purchase = new Purchase();

      if ($user->hasRole('customer')) {
          $request->request->add(['customer' => $user->profile->id]);
      }

      return $this->sendPaginatedResponse(
          $request,
          $purchase->queryPurchases($request),
          Purchases::class
      );
    }

  /**
   * Store a newly created resource in storage.
   *
   * @param CreatePurchaseRequest|Request $request
   *
   * @return PurchaseResource
   */
  public function store(CreatePurchaseRequest $request) {
    $input = $request->except(['product_id', 'store_id']);
    $product = Product::find($request->get('product_id'));
    $store = Store::find($request->get('store_id'));
    $input['points'] = $product ? $product->points : 0;

    $purchase = Purchase::make($input);
    $purchase->next_service = Carbon::parse($request->get('invoice_date'))
      ->addMonths($product->service_time)
      ->toDateString();
    $purchase->customer()->associate($request->user()->profile);
    $purchase->product()->associate($product);
    $purchase->store()->associate($store);
    $warranty = Configuration::get('extended_warranty', NULL);
    $warranty_time = Configuration::get('extended_warranty_time', NULL);

    if ($warranty && $warranty_time) {
      if ($warranty > 0) {
        $purchase->extended_warranty = $warranty;
        $purchase->extended_warranty_time = $warranty_time;
      }
    }


    if ($request->hasFile('invoice')) {
      $path = $request->file('invoice')->store('invoices');
      $purchase->invoice = $path;
    }

    $purchase->save();

    return new PurchaseResource($purchase);
  }

  /**
   * Display the specified resource.
   *
   * @param Purchase $purchase
   *
   * @return PurchaseResource
   */
  public function show(Purchase $purchase) {
    return new PurchaseResource($purchase);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param UpdatePurchaseRequest $request
   * @param Purchase              $purchase
   *
   * @return PurchaseResource
   */
  public function update(UpdatePurchaseRequest $request, Purchase $purchase) {
    $input = $request->except(['product_id', 'store_id', 'customer_id', 'invoice']);
    if ($purchase->status == 'pending') {
      $purchase->fill($input);

      if ($request->has('product_id')) {
        $product = Product::find($request->get('product_id'));
        $purchase->product()->associate($product);
        $purchase->points = $product->points;
        $purchase->next_service = Carbon::parse($purchase->invoice_date)
          ->addMonths($product->service_time)
          ->toDateString();
      }

      if ($request->has('store_id')) {
        $store = Store::find($request->get('store_id'));
        $purchase->store()->associate($store);
      }

      if ($request->hasFile('invoice')) {
        Storage::delete($purchase->getOriginal('invoice'));
        $path = $request->file('invoice')->store('invoices');
        $purchase->invoice = $path;
      }
      $purchase->save();
    }
    return new PurchaseResource($purchase);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Purchase $purchase
   *
   * @return PurchaseController
   */
  public function destroy(Purchase $purchase) {
    $purchase->delete();
    return $this->sendDeletedResponse();
  }

  public function updateStatus(UpdatePurchaseStatusRequest $request, Purchase $purchase) {
    $purchase->status = $request->get('status');
    $purchase->save();
    return new PurchaseResource($purchase);
  }

  public function showUserPurchases(Request $request, Customer $customer) {
    $purchase = new Purchase();
    $request->request->add(['customer' => $customer->id]);
    return $this->sendPaginatedResponse(
      $request, $purchase->queryPurchases($request), CustomerPurchases::class
    );
  }

  public function updateNextService(Request $request, Purchase $purchase) {
    $purchase->next_service = Carbon::now()->addMonths($purchase->product->service_time)->toDateString();
    $purchase->save();
    return new PurchaseResource($purchase);
  }

  public function sendWarranty(Request $request, Purchase $purchase) {
    $user = $purchase->customer->user;
    Mail::to($user)->queue(new \App\Mail\ExtendedWarrantyMail($purchase));
    return $this->sendOkResponse("Mensaje enviado");
  }

  public function myValidPurchases(Request $request){
    $size = $this->getSizeQuery($request);
    $customer = Auth::user()->profile;
    $results = Purchase::query()->where("customer_id", $customer->id)
      ->with("product")
      ->where("status","valid")
      ->paginate($size);
    return new ApiResponseResourceCollection($results);
  }
}
