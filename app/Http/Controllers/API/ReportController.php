<?php

namespace App\Http\Controllers\API;

use App\CreditNote;
use App\Http\Controllers\Controller;
use App\Http\Resources\ApiResponseResourceCollection;
use App\ReportBenefitCustomer;
use App\ReportPurchase;
use Illuminate\Http\Request;

class ReportController extends Controller {

  use IndexControllerTrait;

  function sendCsvResponse($lines, $filename) {
    return response(join("\n", $lines))
      ->withHeaders([
        "Content-Type" => "text/csv",
        "Content-Disposition: attachment; filename=\"$filename\""
      ]);
  }

  function purchases(Request $request) {
    $query = ReportPurchase::getReportQuery($request);
    $results = $query->paginate($request->input('size', config('pagination.size')));

    $data = array_map(
      function ($r) {
        $rp = new ReportPurchase((array) $r);
        $rp->load(["product", "store"]);
        return $rp;
      }, $results->items()
    );

    $meta = [
      "current_page" => $results->currentPage(),
      "from"         => $results->firstItem(),
      "last_page"    => $results->lastPage(),
      "per_page"     => $results->perPage(),
      "to"           => $results->lastItem(),
      "total"        => $results->total(),
    ];

    return compact("data", "meta");
  }

  function customerBenefitByProduct(Request $request) {
    $query = ReportBenefitCustomer::getReportQuery($request, ["p.product_id"], ["p.product_id"]);

    $results = $query->paginate($request->input('size', config('pagination.size')));

    $data = array_map(
      function ($r) {
        $rp = new ReportBenefitCustomer((array) $r);
        $rp->load(["benefit", "product", "workshop"]);
        return $rp;
      }, $results->items()
    );

    $meta = [
      "current_page" => $results->currentPage(),
      "from"         => $results->firstItem(),
      "last_page"    => $results->lastPage(),
      "per_page"     => $results->perPage(),
      "to"           => $results->lastItem(),
      "total"        => $results->total(),
    ];

    return compact("data", "meta");
  }

  function customerBenefitByBenefit(Request $request) {
    $query = ReportBenefitCustomer::getReportQuery($request, ["bc.benefit_id"], ["bc.benefit_id"]);
    $results = $query->paginate($request->input('size', config('pagination.size')));

    $data = array_map(
      function ($r) {
        $rp = new ReportBenefitCustomer((array) $r);
        $rp->load(["benefit", "product", "workshop"]);
        return $rp;
      }, $results->items()
    );

    $meta = [
      "current_page" => $results->currentPage(),
      "from"         => $results->firstItem(),
      "last_page"    => $results->lastPage(),
      "per_page"     => $results->perPage(),
      "to"           => $results->lastItem(),
      "total"        => $results->total(),
    ];

    return compact("data", "meta");
  }

  function creditNotes(Request $request) {
    $query = CreditNote::queryCreditNotes($request);
    return $this->sendPaginatedResponse($request, $query, ApiResponseResourceCollection::class);
  }

  function purchasesExport(Request $request) {
    $query = ReportPurchase::getReportQuery($request);
    $results = $query->get();
    foreach($results as $i => $r){
      $rp = new ReportPurchase((array) $r);
      $rp->load(["product", "store"]);
      $results[$i] = $rp->toArray();
    }

    $lines = array_map(
      function ($row) {
        return join(",", [
          $row["product"]["name"],
          $row["product"]["sku"],
          $row["product"]["model"],
          $row["total"]
        ]);
      },
      $results->toArray()
    );
    return $this->sendCsvResponse($lines, "report_purchases_" . date("YmdHis") . ".csv");
  }

  function creditNotesExport(Request $request) {
    $query = CreditNote::queryCreditNotes($request);
    $results = $query->get();

    $lines = array_map(
      function ($row) {
        return join(
          ",", [
            $row["id"],
            $row["customer_benefit"]["workshop"]["title"],
            $row["customer_benefit"]["benefit"]["name"],
            $row["customer_benefit"]["created_at"],
            $row["status"],
            $row["price"],
          ]
        );
      }, $results->toArray()
    );
    return $this->sendCsvResponse($lines, "report_credit_notes_" . date("YmdHis") . ".csv");
  }

  function customerBenefitByBenefitExport(Request $request) {
    $query = ReportBenefitCustomer::getReportQuery($request, ["bc.benefit_id"], ["bc.benefit_id"]);
    $results = $query->get();

    $data = array_map(
      function ($r) {
        $rp = new ReportBenefitCustomer((array) $r);
        $rp->load(["benefit", "product", "workshop"]);
        return $rp;
      }, $results->toArray()
    );

    $lines = array_map(function($row) {
      return join(",", [
        $row["benefit"]["name"],
        $row["benefit"]["description"],
        $row["benefit"]["level"]["name"],
        $row["total"]
      ]);
    }, $data);

    return $this->sendCsvResponse($lines, "report_customer_benefit_by_benefit" . date("YmdHis") . ".csv");
  }

  function customerBenefitByProductExport(Request $request) {
    $query = ReportBenefitCustomer::getReportQuery($request, ["p.product_id"], ["p.product_id"]);
    $results = $query->get();

    $data = array_map(
      function ($r) {
        $rp = new ReportBenefitCustomer((array) $r);
        $rp->load(["benefit", "product", "workshop"]);
        return $rp;
      }, $results->toArray()
    );

    $lines = array_map(
      function ($row) {
        return join(
          ",", [
          $row["product"]["name"],
          $row["product"]["sku"],
          $row["product"]["model"],
          $row["total"]
        ]
        );
      }, $data
    );

    return $this->sendCsvResponse($lines, "report_customer_benefit_by_product" . date("YmdHis") . ".csv");
  }
}
