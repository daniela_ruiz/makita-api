<?php

namespace App\Http\Controllers\API;

use App\Device;
use App\Http\Requests\Device\CreateDeviceRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
    use IndexControllerTrait;
    /**
     * Display a listing of the resource.
     *
     * @return APIResponseResource
     */
    public function index(Request $request)
    {
        $device = new Device();

        return $this->sendPaginatedResponse(
            $request,
            $device->queryDevices($request),
            ApiResponseResourceCollection::class
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CreateDeviceRequest|Request $request
     * @return APIResponseResource
     */
    public function store(CreateDeviceRequest $request)
    {
        $customer = $request->user()->profile;
        $device = Device::firstOrCreate(
            [
                'device_id' => $request->get('device_id'),
                'customer_id' => $customer->id
            ],
            [
                'active' => true
            ]
        );
        if ($device->wasRecentlyCreated) {
            $firebase = app('firebase');
            $topic = 'alerts';
            $registrationTokens = [ $request->get('device_id') ];
            $firebase
                ->getMessaging()
                ->subscribeToTopic($topic, $registrationTokens);
        }

        return new APIResponseResource($device);
    }

    /**
     * Display the specified resource.
     *
     * @param Device $device
     * @return APIResponseResource
     */
    public function show(Device $device)
    {
        return new APIResponseResource($device);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Device $device
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Device $device)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Device $device
     * @return DeviceController
     */
    public function destroy(Device $device)
    {
        $firebase = app('firebase');
        $topic = 'alerts';
        $registrationTokens = [ $device->device_id ];
        $firebase
            ->getMessaging()
            ->unsubscribeFromTopic($topic, $registrationTokens);

        $device->delete();
        return $this->sendDeletedResponse();
    }
}
