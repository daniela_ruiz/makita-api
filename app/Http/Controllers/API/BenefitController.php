<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\CustomerBenefit;
use App\Events\BenefitApprovedEvent;
use App\Http\Requests\Benefit\ApproveBenefitRequest;
use App\Http\Requests\Reward\CreateBenefitRequest;
use App\Http\Requests\Reward\CreateClaimRequest;
use App\Http\Requests\Reward\UpdateBenefitRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Benefit;
use App\Http\Resources\CustomerBenefitCollection;
use App\Http\Resources\CustomerBenefitResource;
use App\Level;
use App\Purchase;
use App\Workshop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class BenefitController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource|\App\Http\Resources\ApiResponseResourceCollection
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $benefit = new Benefit();

        if ($user->hasRole('customer')) {
            $request->request->add(['level' => $user->profile->points]);
            $request->request->add(['customer' => $user->profile->id]);
        }

        return $this->sendPaginatedResponse(
            $request,
            $benefit->queryBenefits($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateBenefitRequest|Request $request
     * @return APIResponseResource
     */
    public function store(CreateBenefitRequest $request)
    {
        $data = $request->except(['level_id']);
        $benefit = new Benefit();
        $benefit->fill($data);
        $benefit->level()->associate(Level::find($request->get('level_id')));
        $benefit->save();

        return new APIResponseResource($benefit);
    }

    /**
     * Display the specified resource.
     *
     * @param Benefit $benefit
     * @return APIResponseResource
     */
    public function show(Benefit $benefit)
    {
        return new APIResponseResource($benefit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBenefitRequest $request
     * @param Benefit $benefit
     * @return APIResponseResource
     * @internal param Manufacturer $manufacturer
     */
    public function update(UpdateBenefitRequest $request, Benefit $benefit)
    {
        $input = $request->except(['level_id']);

        if ($request->has('level_id')) {
            $level = Level::find($request->get('level_id'));
            $benefit->level()->associate($level);
        }
        $benefit->fill($input);
        $benefit->save();

        return new APIResponseResource($benefit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Benefit $benefit
     * @return BenefitController
     * @internal param Manufacturer $manufacturer
     */
    public function destroy(Benefit $benefit)
    {
        $benefit->delete();
        return $this->sendDeletedResponse();
    }

    public function claim(CreateClaimRequest $request)
    {
        $customer = $request->user()->profile;
        $benefit = Benefit::find($request->get('benefit_id'));

        if ($customer->points >= $benefit->level->points) {
            $workshop = Workshop::find($request->get('workshop_id'));
            $purchase = Purchase::find($request->get('purchase_id'));
            $claim = new CustomerBenefit();
            $claim->purchase()->associate($purchase);
            $claim->benefit()->associate($benefit);
            $claim->workshop()->associate($workshop);
            $claim->customer()->associate($customer);
            $claim->save();

            return new CustomerBenefitResource($claim);
        } else {
            return $this->sendErrorResponse(Lang::get('app_errors.insufficient'));
        }
    }

    public function approveBenefit(ApproveBenefitRequest $request, CustomerBenefit $customerBenefit)
    {
        $price = $request->get("price", 0);
        $oldValue = $customerBenefit->approved;
        $customerBenefit->approved = true;
        $customerBenefit->price = $price;
        $customerBenefit->save();

        if ($oldValue !== $customerBenefit->approved) {
            event(new BenefitApprovedEvent($customerBenefit, $price));
        }

        return new CustomerBenefitResource($customerBenefit);
    }

    public function deleteClaimed(CustomerBenefit $customerBenefit)
    {
        $customerBenefit->delete();

        return $this->sendDeletedResponse();
    }

    public function claimedBenefits(Request $request, Customer $customer = null)
    {
        if ($request->user()->hasRole('customer')) {
            $customer = $request->user()->profile;
        }
        if (!is_null($customer)) {
            $sort = ['created_at', 'DESC'];

            if ($request->has('sort') && !is_null($request->input('sort'))) {
                $sort = explode('|', urldecode($request->input('sort')), 2);
            }
            return $this->sendPaginatedResponse(
                $request,
                $customer->benefits()->orderBy($sort[0], $sort[1]),
                CustomerBenefitCollection::class
            );
        } else {
            $benefit = new CustomerBenefit();
            return $this->sendPaginatedResponse(
                $request,
                $benefit->queryCustomerBenefits($request),
                CustomerBenefitCollection::class
            );
        }
    }

    public function claimedBenefit(CustomerBenefit $customerBenefit)
    {
        return new CustomerBenefitResource($customerBenefit);
    }
}
