<?php

namespace App\Http\Controllers\API;

use App\CreditNote;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreditNote\UpdateCreditNoteRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use Illuminate\Http\Request;

class CreditNoteController extends Controller {

  use IndexControllerTrait;

  function index(Request $request) {
    $query = CreditNote::queryCreditNotes($request);
    return $this->sendPaginatedResponse($request, $query, ApiResponseResourceCollection::class);
  }

  function show(CreditNote $creditNote) {
    return new APIResponseResource($creditNote);
  }

  function update(UpdateCreditNoteRequest $request, CreditNote $creditNote) {
    $creditNote->update($request->all());
    $creditNote->save();
    return new APIResponseResource($creditNote);
  }

  function destroy(CreditNote $creditNote){
    $creditNote->delete();
    return $this->sendDeletedResponse();
  }

  function cancel(CreditNote $creditNote){
    $creditNote->status = "canceled";
    $creditNote->save();
    return new APIResponseResource($creditNote);
  }

  function process(CreditNote $creditNote){
    $creditNote->status = "processed";
    $creditNote->save();
    return new APIResponseResource($creditNote);
  }
}
