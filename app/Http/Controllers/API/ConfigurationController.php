<?php

namespace App\Http\Controllers\API;

use App\Configuration;
use App\Http\Requests\Configuration\UpdateConfigurationRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigurationController extends Controller
{
    use IndexControllerTrait;
    /**
     * Display a listing of the resource.
     *
     * @return ApiResponseResourceCollection | APIResponseResource
     */
    public function index(Request $request)
    {
        $configuration = new Configuration();

        return $this->sendPaginatedResponse(
            $request,
            $configuration->queryConfig($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Configuration  $configuration
     * @return APIResponseResource
     */
    public function show(Configuration $configuration)
    {
        return new APIResponseResource($configuration);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateConfigurationRequest|Request $request
     * @param  \App\Configuration $configuration
     * @return APIResponseResource
     */
    public function update(UpdateConfigurationRequest $request, Configuration $configuration)
    {
        $configuration->update($request->all());
        return new APIResponseResource($configuration);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Configuration  $configuration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Configuration $configuration)
    {
        //
    }
}
