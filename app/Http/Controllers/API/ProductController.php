<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Requests\Manufacturer\UpdateSegmentRequest;
use App\Http\Requests\Product\AddProductImage;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\Products;
use App\Product;
use App\ProductImage;
use App\Segment;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    use IndexControllerTrait, SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource|\App\Http\Resources\ApiResponseResourceCollection
     */
    public function index(Request $request)
    {
        $product = new Product();

        return $this->sendPaginatedResponse(
            $request,
            $product->queryProducts($request),
            Products::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateProductRequest|Request $request
     * @return ProductResource
     */
    public function store(CreateProductRequest $request)
    {
//        $data = $request->except(['category_id', 'segment_id', 'image']);
        $data = $request->except(['category_id', 'image']);
        $product = new Product();
        $product->fill($data);

        $product->save();

        for ($i = 1; $i <= 5; $i++) {
            if ($request->hasFile('image'. "{$i}")) {
                $path = $request->file('image'. "{$i}")->store('products');
                $product->images()->create(['image' => $path]);
            }
        }

        $category = Category::find($request->get('category_id'));
//        $segment = Segment::find($request->get('segment_id'));
        if ($category) {
            $product->category()->associate($category)->save();
        }
//        if ($segment) {
//            $product->segment()->associate($segment)->save();
//        }

        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return ProductResource
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param Product $product
     * @return ProductResource
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
//        $product->fill($request->except(['category_id', 'segment_id', 'image']));
        $product->fill($request->except(['category_id', 'image']));

//        if ($request->hasFile('image')) {
//            Storage::delete($product->getOriginal('image'));
//            $path = $request->file('image')->store('products');
//            $product->image = $path;
//        }
        if ($request->hasFile('image')) {
            if ($product->image) {
                Storage::delete($product->getOriginal('image'));
                $product->image = null;
            }
            if ($product->images) {
                foreach ($product->images as $old) {
                    Storage::delete($old->getOriginal('image'));
                }
                $product->images()->delete();
            }
            foreach ($request->file('image') as $image) {
                $path = $image->store('products');
                $product->images()->create(['image' => $path]);
            }
        }
        $product->save();

        if ($request->has('category_id')) {
            if ($request->get('category_id') !== $product->category_id) {
                $category = Category::find($request->get('category_id'));
                if ($category) {
                    $product->category()->associate($category)->save();
                }
            }
        }
//        if ($request->has('segment_id')) {
//            if ($request->get('segment_id') !== $product->segment_id) {
//                $segment = Segment::find($request->get('segment_id'));
//                if ($segment) {
//                    $product->segment()->associate($segment)->save();
//                }
//            }
//        }
        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return ProductController
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return $this->sendDeletedResponse();
    }

    public function addImage(AddProductImage $request, Product $product)
    {
        $path = $request->file('image')->store('products');
        $product->images()->create(['image' => $path])->save();

        return new ProductResource($product);
    }

    public function deleteImage(ProductImage $productImage)
    {
        Storage::delete($productImage->getOriginal('image'));
        $productImage->delete();

        return new ProductResource($productImage->product);
    }
}
