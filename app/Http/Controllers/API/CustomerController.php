<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\UpdateCustomerRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\PurchaseResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\Users;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \App\Http\Resources\APIResponseResource
     */
    public function index(Request $request)
    {
        $customer = new Customer();

        return $this->sendPaginatedResponse(
            $request,
            $customer->queryCustomers($request),
            Users::class
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Customer $customer
     * @return APIResponseResource
     * @internal param \App\User $user
     */
    public function show(Customer $customer)
    {
        return new APIResponseResource($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCustomerRequest|Request $request
     * @param Customer $customer
     * @return APIResponseResource
     */
    public function update(UpdateCustomerRequest $request, Customer $customer)
    {
        $input = $request->all();
        if (array_key_exists('password', $input)) {
            $input['password'] = Hash::make($input['password']);
        }
        $customer->update($input);
        $customer->user->update($input);

        return new APIResponseResource($customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Customer $customer
     * @return CustomerController
     * @internal param Customer $user
     */
    public function destroy(Customer $customer)
    {
        $user = $customer->user;
        $customer->devices()->delete();
        $user->roles()->detach();
        $customer->delete();
        $user->delete();
        return $this->sendDeletedResponse();
    }
}
