<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

trait IndexControllerTrait
{
    private function getSizeQuery(Request $request)
    {
        return $request->input('size', config('pagination.size'));
    }

    protected function sendDeletedResponse()
    {
        return (new APIResponseResource(
            collect(['code' => Response::HTTP_NO_CONTENT, 'message' => null])
        ))->response()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    protected function sendOkResponse($message = '')
    {
        return (new APIResponseResource(
            collect(['code' => Response::HTTP_OK, 'message' => $message])
        ))->response()->setStatusCode(Response::HTTP_OK);
    }
    protected function sendErrorResponse($message = "")
    {
        return (new APIResponseResource(
            collect(['code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'message' => $message])
        ))->response()->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
    protected function sendBadRequestResponse($message = "")
    {
        return (new APIResponseResource(
            collect(['code' => Response::HTTP_BAD_REQUEST, 'message' => $message])
        ))->response()->setStatusCode(Response::HTTP_BAD_REQUEST);
    }

    protected function sendPaginatedResponse(Request $request, $query, $class)
    {
        $size = $this->getSizeQuery($request);
        if ($size == 0) {
            return new APIResponseResource(($query->get()));
        } else {
            $results = $query->paginate($size);
            $results->appends($request->except('page'))->links();
            return new $class($results);
        }
    }
}
