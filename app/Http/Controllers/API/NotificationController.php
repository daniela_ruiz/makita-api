<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Requests\Notification\CreateNotificationRequest;
use App\Http\Requests\Notification\UpdateNotificationRequest;
use App\Http\Requests\Subscription\SubscriptionRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Kreait\Firebase\Messaging\ApnsConfig;

class NotificationController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \App\Http\Resources\APIResponseResource
     */
    public function index(Request $request)
    {
        $notification = new Notification();

        return $this->sendPaginatedResponse(
            $request,
            $notification->queryNotifications($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateNotificationRequest|Request $request
     * @return APIResponseResource
     */
    public function store(CreateNotificationRequest $request)
    {
        $data = $request->except(['image']);
        $notification = new Notification();
        $notification->fill($data);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('notifications');
            $notification->image = $path;
        }
        $notification->save();

        return new APIResponseResource($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Notification $notification
     * @return APIResponseResource
     */
    public function show(Notification $notification)
    {
        return new APIResponseResource($notification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNotificationRequest  $request
     * @param  \App\Notification $notification
     * @return APIResponseResource
     */
    public function update(UpdateNotificationRequest $request, Notification $notification)
    {
        $data = $request->except(['image']);
        $notification->fill($data);
        if ($request->hasFile('image')) {
            Storage::delete($notification->getOriginal('image'));
            $path = $request->file('image')->store('notifications');
            $notification->image = $path;
        }
        $notification->save();

        return new APIResponseResource($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Notification $notification
     * @return NotificationController
     * @internal param \App\Category $category
     */
    public function destroy(Notification $notification)
    {
        $notification->delete();
        return $this->sendDeletedResponse();
    }

    public function sendNotification(Notification $notification)
    {
        $firebase = app('firebase');

        $message = [
            "id" => (string) $notification->id,
            "type" => (string) $notification->type,
            "title" => (string) $notification->title,
            "message" => (string) $notification->description,
            "image" => (string) $notification->image,
            "date" => (string) $notification->date
        ];

        $firebase->getMessaging()->send([
            'topic' => 'alerts',
            'data' => $message,
            'apns' => [
                'headers' => [
                    'apns-priority' => '10',
                ],
                'payload' => [
                    'aps' => [
                        'alert' => [
                            'title' => $message["type"],
                            'subtitle' => $message["title"],
                            'body' => $message["message"],
                            'launch-image' => $message["image"]
                        ]
                    ],
                ],
            ],
        ]);
        Customer::where('subscription', '=', true)->has('user')->get()->each(function ($customer) use ($notification) {
            if ($customer->user->email) {
                Mail::to($customer->user)->queue(new \App\Mail\Notification($notification, $customer->id));
            }
        });

        return $this->sendOkResponse("Notification sent");
    }

    public function subscribe(Request $request)
    {
        $customer = $request->user()->profile;
        $customer->subscription = true;
        $customer->save();
        return new APIResponseResource($customer);
    }

    public function unsubscribe(Customer $customer)
    {
        $customer->subscription = false;
        $customer->save();
        return view("emails.subscription", ['subscribe'=> $customer->id]);
    }
}
