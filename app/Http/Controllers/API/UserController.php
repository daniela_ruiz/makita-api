<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\UpdateCustomerRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\UpdateUsersPasswordRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\Users;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource
     */
    public function index(Request $request)
    {
      $user = new User();
      $query = $user->queryUsers($request);

      $currentUser = $request->user();
      if ($currentUser->hasRole("operator") && !$currentUser->hasRole("admin")){
        $workshop = $currentUser->workshop();
        if (!is_null($workshop)) {
          $query->join("role_user", "role_user.user_id", "=", "users.id")->where("role_user.workshop_id", $workshop);
        }
      }

      return $this->sendPaginatedResponse(
            $request, $query,
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest|Request $request
     * @return UserResource
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user =  User::create($input);
        if ($request->has('roles')) {
            $roles = $request->input("roles");
            $user->roles()->attach($roles);
        }
        if ($request->has("workshop_id")){
          $operatorRole = Role::findByName("operator");
          DB::table("role_user")->where("user_id",$user->id)
            ->where("role_id",$operatorRole->id)
            ->update(["workshop_id" => $request->input("workshop_id")]);
        }
        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest|Request $request
     * @param  \App\User $user
     * @return UserResource
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $input = $request->all();
        if (array_key_exists('password', $input)) {
            $input['password'] = Hash::make($input['password']);
        }
        $user->update($input);
        if ($request->has('roles')) {
            $roles = $request->get('roles');
            $user->roles()->sync($roles);
        }

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return UserController
     */
    public function destroy(User $user)
    {
        $user->delete();
        return $this->sendDeletedResponse();
    }

    public function profile(Request $request)
    {
        $user = $request->user();
        if ($user->hasRole('customer')) {
            return new CustomerResource($user);
        } else {
            return new APIResponseResource($user);
        }
    }

    public function updateProfile(UpdateCustomerRequest $request)
    {
        $user = $request->user();
        $input = $request->except('password');
        $user->update($input);
        if ($user->profile) {
             $user->profile->update($input);
        }
        if ($user->hasRole('customer')) {
            return new CustomerResource($user);
        } else {
            return new APIResponseResource($user);
        }
    }

    public function changePassword(UpdateUsersPasswordRequest $request)
    {
        $user = $request->user();
        $input = $request->only(['password']);
        if (array_key_exists('password', $input)) {
            $input['password'] = Hash::make($input['password']);
        }
        $user->update($input);

        if ($user->hasRole('customer')) {
            return new CustomerResource($user);
        } else {
            return new APIResponseResource($user);
        }
    }

    public function showVideo()
    {
        $value = "vid/video";
        return env('APP_ENV') == 'production' ? secure_url(Storage::url($value))
            : url(Storage::url($value));
    }
}
