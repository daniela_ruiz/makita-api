<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\Store\CreateStoreRequest;
use App\Http\Requests\Store\UpdateStoreRequest;
use App\Http\Requests\Store\UpdateWorkshopRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Store;
use App\Workshop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class StoreController extends Controller
{
    use IndexControllerTrait;
    /**
     * Display a listing of the resource.
     *
     * @return APIResponseResource
     */
    public function index(Request $request)
    {
        $store = new Store();

        return $this->sendPaginatedResponse(
            $request,
            $store->queryStores($request),
            ApiResponseResourceCollection::class
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CreateStoreRequest|Request $request
     * @return APIResponseResource
     */
    public function store(CreateStoreRequest $request)
    {
        $store = Store::create($request->all());
        return new APIResponseResource($store);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return APIResponseResource
     */
    public function show(Store $store)
    {
        return new APIResponseResource($store);
    }

    public function update(UpdateStoreRequest $request, Store $store)
    {
        $store->update($request->all());
        return new APIResponseResource($store);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return StoreController
     */
    public function destroy(Store $store)
    {
        $store->delete();
        return $this->sendDeletedResponse();
    }

    public function locations(Request $request)
    {
        $locations = collect();
        $locations->put('stores', Store::all());
        $locations->put('workshops', Workshop::all());
        return new APIResponseResource($locations);
    }
}
