<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\Workshop\CreateWorkshopRequest;
use App\Http\Requests\Workshop\UpdateWorkshopRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Http\Resources\UserResource;
use App\Role;
use App\User;
use App\Workshop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class WorkshopController extends Controller {

  use IndexControllerTrait;

  /**
   * Display a listing of the resource.
   *
   * @return APIResponseResource
   */
  public function index(Request $request) {
    $workshop = new Workshop();

    return $this->sendPaginatedResponse(
      $request, $workshop->queryWorkshops($request), ApiResponseResourceCollection::class
    );
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param CreateWorkshopRequest|Request $request
   *
   * @return APIResponseResource
   */
  public function store(CreateWorkshopRequest $request) {
    $workshop = Workshop::create($request->all());
    return new APIResponseResource($workshop);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Workshop $workshop
   *
   * @return APIResponseResource
   */
  public function show(Workshop $workshop) {
    return new APIResponseResource($workshop);
  }

  public function update(UpdateWorkshopRequest $request, Workshop $workshop) {
    $workshop->update($request->all());
    return new APIResponseResource($workshop);
  }

  public function destroy(Workshop $workshop) {
    $workshop->delete();
    return $this->sendDeletedResponse();
  }

  public function createUser(CreateUserRequest $request) {
    $input = $request->all();
    $input['password'] = Hash::make($input['password']);
    $user = User::create($input);

    $operatorRole = Role::findByName("operator");

    $workshop = $user->roles()->where("name", "operator")
      ->withPivot("workshop_id")
      ->first()->pivot->workshop_id;

    $user->roles()->attach($operatorRole, ['workshop_id' => $workshop]);

    return new UserResource($user);
  }

  public function addUser(User $user) {
    $workshop = $user->roles()->where("name", "operator")
      ->withPivot("workshop_id")
      ->first()->pivot->workshop_id;

    $operatorRole = Role::findByName("operator");

    $user->roles()->syncWithoutDetaching([
      $operatorRole->id => ['workshop_id' => $workshop]
    ]);

    return new UserResource($user);
  }
}
