<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\Invoice\CreateInvoiceRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Invoice\UpdateInvoiceRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource|\App\Http\Resources\ApiResponseResourceCollection
     */
    public function index(Request $request)
    {
        $invoice = new Invoice();

        return $this->sendPaginatedResponse(
            $request,
            $invoice->queryInvoices($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateInvoiceRequest|Request $request
     * @return APIResponseResource
     */
    public function store(CreateInvoiceRequest $request)
    {
        $data = $request->all();
        $invoice = new Invoice();
        $invoice->fill($data);
        $invoice->save();

        return new APIResponseResource($invoice);
    }

    /**
     * Display the specified resource.
     *
     * @param Invoice $invoice
     * @return APIResponseResource
     * @internal param Manufacturer $manufacturer
     */
    public function show(Invoice $invoice)
    {
        return new APIResponseResource($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateInvoiceRequest $request
     * @param Invoice $invoice
     * @return APIResponseResource
     */
    public function update(UpdateInvoiceRequest $request, Invoice $invoice)
    {
        $invoice->update($request->all());

        return new APIResponseResource($invoice);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Invoice $invoice
     * @return InvoiceController
     */
    public function destroy(Invoice $invoice)
    {
        $invoice->delete();
        return $this->sendDeletedResponse();
    }
}
