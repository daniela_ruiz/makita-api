<?php

namespace App\Http\Controllers\API;

use App\CustomerBenefit;
use App\Purchase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function getDashboard()
    {
        $dashboard['products'] = Purchase::getMostSoldProducts();
        $dashboard['benefits'] = CustomerBenefit::getMostClaimedBenefits();
        $dashboard['workshops'] = CustomerBenefit::getMostVisitedWorkshops();
        return $dashboard;
    }
}
