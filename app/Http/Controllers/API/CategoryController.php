<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource|\App\Http\Resources\ApiResponseResourceCollection
     */
    public function index(Request $request)
    {
        $category = new Category();

        return $this->sendPaginatedResponse(
            $request,
            $category->queryCategories($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest  $request
     * @return APIResponseResource
     */
    public function store(CreateCategoryRequest $request)
    {
        $data = $request->all('name');
        $category = new Category();
        $category->fill($data);
        $category->save();

        return new APIResponseResource($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return APIResponseResource
     */
    public function show(Category $category)
    {
        return new APIResponseResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCategoryRequest  $request
     * @param  \App\Category  $category
     * @return APIResponseResource
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        return new APIResponseResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return CategoryController
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return $this->sendDeletedResponse();
    }
}
