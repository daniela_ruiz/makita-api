<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource
     */
    public function index(Request $request)
    {
        $role = new Role();

        return $this->sendPaginatedResponse(
            $request,
            $role->queryRoles($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRoleRequest|Request $request
     * @return APIResponseResource
     */
    public function store(CreateRoleRequest $request)
    {
        $data = $request->all();
        $role = new Role();
        $role->fill($data);
        $role->save();

        return new APIResponseResource($role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return APIResponseResource
     */
    public function show(Role $role)
    {
        return new APIResponseResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRoleRequest  $request
     * @param  \App\Role  $role
     * @return APIResponseResource
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $role->update($request->all());

        return new APIResponseResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return RoleController
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return $this->sendDeletedResponse();
    }
}
