<?php

namespace App\Http\Controllers\API;

use App\Catalogue;
use App\Customer;
use App\Http\Requests\Catalogue\CreateCatalogueRequest;
use App\Http\Requests\Catalogue\UpdateCatalogueRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class CatalogueController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \App\Http\Resources\APIResponseResource
     */
    public function index(Request $request)
    {
        $catalogue = new Catalogue();

        return $this->sendPaginatedResponse(
            $request,
            $catalogue->queryCatalogues($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCatalogueRequest $request
     * @return APIResponseResource
     * @internal param $ |Request $request
     */
    public function store(CreateCatalogueRequest $request)
    {
        $data = $request->except(['image', 'file']);
        $catalogue = new Catalogue();
        $catalogue->fill($data);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('catalogues');
            $catalogue->image = $path;
        }
        if ($request->hasFile('file')) {
            $path = $request->file('file')->store('catalogues');
            $catalogue->file = $path;
        }
        $catalogue->save();

        return new APIResponseResource($catalogue);
    }

    /**
     * Display the specified resource.
     *
     * @param Catalogue $catalogue
     * @return APIResponseResource
     * @internal param Notification $notification
     */
    public function show(Catalogue $catalogue)
    {
        return new APIResponseResource($catalogue);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCatalogueRequest  $request
     * @param  \App\Catalogue $catalogue
     * @return APIResponseResource
     */
    public function update(UpdateCatalogueRequest $request, Catalogue $catalogue)
    {
        $data = $request->except(['image', 'file']);
        $catalogue->fill($data);

        if ($request->hasFile('image')) {
            Storage::delete($catalogue->getOriginal('image'));
            $path = $request->file('image')->store('catalogues');
            $catalogue->image = $path;
        }
        if ($request->hasFile('file')) {
            Storage::delete($catalogue->getOriginal('file'));
            $path = $request->file('file')->store('catalogues');
            $catalogue->file = $path;
        }
        $catalogue->save();

        return new APIResponseResource($catalogue);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Catalogue $catalogue
     * @return CatalogueController
     * @internal param Catalogue $notification
     */
    public function destroy(Catalogue $catalogue)
    {
        $catalogue->delete();
        return $this->sendDeletedResponse();
    }

}
