<?php

namespace App\Http\Controllers\API;

use App\Segment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Segment\UpdateSegmentRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\ApiResponseResourceCollection;
use Illuminate\Http\Request;

class SegmentController extends Controller
{
    use IndexControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return APIResponseResource|\App\Http\Resources\ApiResponseResourceCollection
     */
    public function index(Request $request)
    {
        $segment = new Segment();

        return $this->sendPaginatedResponse(
            $request,
            $segment->querySegments($request),
            ApiResponseResourceCollection::class
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return APIResponseResource
     */
    public function store(Request $request)
    {
        $data = $request->all('name');
        $segment = new Segment();
        $segment->fill($data);
        $segment->save();

        return new APIResponseResource($segment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Segment  $segment
     * @return APIResponseResource
     */
    public function show(Segment $segment)
    {
        return new APIResponseResource($segment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateSegmentRequest  $request
     * @param  \App\Segment  $segment
     * @return APIResponseResource
     */
    public function update(UpdateSegmentRequest $request, Segment $segment)
    {
        $segment->update($request->all());

        return new APIResponseResource($segment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Segment $segment)
    {
        $segment->delete();
        return $this->sendDeletedResponse();
    }
}
