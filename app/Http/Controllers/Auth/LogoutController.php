<?php
/**
 * Created by PhpStorm.
 * User: apokdev
 * Date: 8/31/18
 * Time: 4:56 AM
 */

namespace App\Http\Controllers\Auth;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LogoutController
{
    public function logout()
    {
        if (Auth::check()) {
            $user = Auth::user();

            $accessToken = $user->token();

            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                    'revoked' => true
                ]);
            $accessToken->revoke();
        }
    }
}