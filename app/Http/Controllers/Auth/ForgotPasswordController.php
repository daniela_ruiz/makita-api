<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\RequestsAuth\ForgotPasswordRequest;
use App\Http\Resources\APIResponseResource;
//use App\Notifications\ForgotPasswordNotification;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * @param ForgotPasswordRequest $request
     * @return APIResponseResource
     */
    public function sendResetLinkEmail(ForgotPasswordRequest $request)
    {
        $user = User::where('email', $request->input('email'))->first();

        $password = str_random(8);

        $user->password = Hash::make($password);

        $user->save();

        Mail::to($user)->send(new \App\Mail\ForgotPassword($password, $user->email));

        return new APIResponseResource(collect(['message' => Lang::get('passwords.sent')]));
    }
}
