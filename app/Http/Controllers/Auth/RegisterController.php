<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\APIResponseResource;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\UserResource;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation.
    |
    */

    /**
     * Create a new user instance after a valid registration.
     *
     * @param RegisterRequest $request
     * @return CustomerResource
     * @internal param array $data
     */
    protected function create(RegisterRequest $request)
    {
        $user =  User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $user->profile()->create($request->all());
        $user->profile->level()->associate($user->profile->getLevel())->save();
        $user->roles()->attach(Role::findByName('customer'));
        $user->load('profile');
        $user->load('roles');

        return new CustomerResource($user);
    }
}
