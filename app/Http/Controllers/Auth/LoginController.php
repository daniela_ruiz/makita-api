<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Traits\PassportTrait;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use League\OAuth2\Server\Exception\OAuthServerException;

class LoginController extends Controller
{
    use PassportTrait;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function login(LoginRequest $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            $user = Auth::user();

            return $this->authenticated($user);
        } else {
            throw new AuthenticationException(Lang::get('auth.failed'));
        }
    }

    protected function authenticated($user)
    {
        $oauth_client = DB::table('oauth_clients')->first();

        if (is_null($oauth_client)) {
            throw OAuthServerException::invalidClient();
        }
        return $this->getBearerTokenByUser($user, $oauth_client->id, false);
    }

    public function refreshToken(RefreshTokenRequest $request)
    {
        $refreshToken = $request->get('refresh_token');

        return $this->refreshUserToken($refreshToken);
    }
}
