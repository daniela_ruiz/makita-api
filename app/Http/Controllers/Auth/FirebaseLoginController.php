<?php

namespace App\Http\Controllers\Auth;

use App\Customer;
use App\Http\Controllers\API\IndexControllerTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\FirebaseLoginRequest;
use App\Role;
use App\Traits\PassportTrait;
use App\User;
use Firebase\Auth\Token\Exception\InvalidToken;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use League\OAuth2\Server\Exception\OAuthServerException;
use Illuminate\Support\Facades\Log;

class FirebaseLoginController extends Controller
{
    use PassportTrait, IndexControllerTrait;

    public function login(FirebaseLoginRequest $request)
    {
        $oauth_client = DB::table('oauth_clients')->first();
        if (is_null($oauth_client)) {
            throw OAuthServerException::invalidClient();
        } else {
            $firebase = app('firebase');
            $idTokenString = $request->get('firebaseToken');
            try {
                $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString, false, true);
            } catch (InvalidToken $e) {
                return new response(
                    collect(['code' => Response::HTTP_UNAUTHORIZED, 'message' => $e->getMessage()]),
                    Response::HTTP_UNAUTHORIZED
                );
            }
            $uid = $verifiedIdToken->getClaim('sub');

            $user = $firebase->getAuth()->getUser($uid);

            if (isset($user->email)) {
                $dbUser = User::where('email', $user->email)->first();

                if (is_null($dbUser)) {
                    $word = str_random(6);
                    $dbUser =  User::create([
                        'name' => $user->displayName,
                        'email' => $user->email,
                        'password' => Hash::make($word),
                    ]);

                    $dbUser->profile()->create([
                        'birth_date' => null,
                        'gender' => null,
                        'firebase_uid' => $user->uid
                    ]);
                    $dbUser->profile->level()->associate($dbUser->profile->getLevel())->save();

                    $dbUser->roles()->attach(Role::findByName('customer'));
                }

                return $this->getBearerTokenByUser($dbUser, $oauth_client->id, false);
            }
            return $this->sendErrorResponse("No pudimos obtener sus datos");
        }
    }
}
