<?php

namespace App;

use App\Traits\Enums;
use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Catalogue extends Model
{
    use ModelQueryTrait, Enums;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'title',
        'description',
        'file',
        'image',
        'active'
    ];

    protected $attributes = [
        'active' => true
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    public $search_attributes = [
        'title',
        'description',
    ];

    public function getImageAttribute($value)
    {
        return !is_null($value) ? env('APP_ENV') == 'production' ? secure_url(Storage::url($value))
            : url(Storage::url($value)) : null;
    }

    public function getFileAttribute($value)
    {
        return !is_null($value) ? env('APP_ENV') == 'production' ? secure_url(Storage::url($value))
            : url(Storage::url($value)) : null;
    }

    public function queryCatalogues(Request $request)
    {
        $query = $this->modelQuery(Catalogue::class, $request, $this->search_attributes);

        return $query;
    }
}
