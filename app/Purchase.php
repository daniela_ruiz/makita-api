<?php

namespace App;

use App\Traits\Enums;
use App\Traits\ModelQueryTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Purchase extends Model
{
    use ModelQueryTrait, Enums, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_number',
        'invoice_date',
        'invoice',
        'points',
        'customer_id',
        'product_id',
        'store_id',
        'serial'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'customer_id' => 'integer',
        'product_id' => 'integer',
        'store_id' => 'integer'
    ];

    private $search_attributes = [
        'invoice_number',
        'invoice_date',
        'serial'
    ];

    protected $attributes = [
        'status' => 'pending'
    ];

    // Define all of the valid options in an array as a protected property that
    //    starts with 'enum' followed by the plural studly cased field name
    protected $enumStatuses = [
        'p' => 'pending',
        'v' => 'valid',
        'i' => 'invalid',
    ];

    /**
     * The products that belong to the purchase.
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }

    public function getInvoiceAttribute($value)
    {
        return !is_null($value) ? env('APP_ENV') == 'production' ?
            secure_url(Storage::url($value)) : url(Storage::url($value)) : null;
    }

    public function scopeByCustomer($query, $filter)
    {
        $user = new User();
        $attributes = $user->search_attributes;
        $query->orWhereHas('customer.user', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    $query->orWhere($attribute, 'like', "%$filter%");
                }
            });
        });
        return $query;
    }

    public function scopeByProduct($query, $filter)
    {
        $product = new Product();
        $attributes = $product->search_attributes;
        $query->orWhereHas('product', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    $query->orWhere($attribute, 'like', "%$filter%");
                }
            });
        });
        return $query;
    }

    public function scopeByStore($query, $filter)
    {
        $store = new Store();
        $attributes = $store->search_attributes;
        $query->orWhereHas('store', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    $query->orWhere($attribute, 'like', "%$filter%");
                }
            });
        });
        return $query;
    }

    public function queryPurchases(Request $request)
    {
        if (!$request->has('sort')) {
            $request->request->add(['sort' => 'created_at|DESC']);
        }

        $filter = $request->query('filter', null);
        $from = $request->query('from', null);
        $to = $request->query('to', null);
        $orderBy = $this->getSortQuery($request);
        $attributes = $this->search_attributes;

        $query = Purchase::with('customer')->where(
            function ($query) use ($filter, $attributes) {
                if ($filter) {
                    $query->where(function ($query) use ($filter, $attributes) {
                        foreach ($attributes as $attribute) {
                            $query->orWhere($attribute, 'like', "%$filter%");
                        }
                    });
                }
            }
        );
        if (!empty($from) && !empty($to)) {
            $dateFrom = Carbon::createFromFormat('Y-m-d', $from)->hour(00)->minute(00)->second(00);
            $dateTo = Carbon::createFromFormat('Y-m-d', $to)->hour(23)->minute(59)->second(59);

            $query->whereBetween('created_at', [$dateFrom, $dateTo]);
        }

        if ($request->has('customer')) {
            $query->where('customer_id', $request->get('customer'));
        }
        if ($request->has('product')) {
          $query->where('product_id', $request->get('product'));
        }
        if ($request->has('store')) {
          $query->where('store_id', $request->get('store'));
        }

        if ($request->has('filter')) {
            $filter = $request->get('filter');
            $query->byCustomer($filter);
            $query->byProduct($filter);
            $query->byStore($filter);
        }
        $query->join(DB::raw("(SELECT users.name as 'username', customers.id as 'id' FROM customers JOIN users ON customers.user_id = users.id where customers.deleted_at is null) customer"), 'purchases.customer_id', '=', 'customer.id')
            ->join('products', 'purchases.product_id', '=', 'products.id')
            ->select('purchases.*', 'customer.username', 'products.name as product_name')
            ->orderBy($orderBy[0], $orderBy[1]);

        return $query;
    }

    public static function getMostSoldProducts()
    {
        $purchases =  Purchase::with('product')->
        select('product_id', DB::raw('count(*) as total'))
            ->groupBy('product_id')
            ->orderBy('total', 'DESC')
            ->limit(5)
            ->get();
        return $purchases;
    }
}
