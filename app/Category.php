<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Category extends Model
{
    use ModelQueryTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public $search_attributes = ['name'];

    public function queryCategories(Request $request)
    {
        if (!$request->has('sort')) {
            $request->request->add(['sort' => 'name|ASC']);
        }
        $query = $this->modelQuery(Category::class, $request, $this->search_attributes);

        return $query;
    }
}
