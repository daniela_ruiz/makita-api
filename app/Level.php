<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Level extends Model
{
    use ModelQueryTrait;
    protected $fillable = [
        'name',
        'slug',
        'points',
        'multiplier'
    ];

    protected $search_attributes = [
        'name',
        'slug',
        'points'
    ];

    protected $casts = [
        'points' => 'integer',
        'multiplier' => 'integer'
    ];

    public function getImageAttribute($value)
    {
        return !is_null($value) ? env('APP_ENV') == 'production' ? secure_url(Storage::url($value)) : url(Storage::url($value)) : null;
    }

    public static function calculateCustomerLevel(Customer $customer)
    {
        return Level::where('points', '<=', $customer->points)->orderBy('points', 'desc')->first();
    }
    public static function calculateLevel($points)
    {
        return Level::where('points', '<=', $points)->orderBy('points', 'desc')->first();
    }
    public function queryLevels(Request $request)
    {
        if (!$request->has('sort')) {
            $request->request->add(['sort' => 'points|ASC']);
        }
        $query = $this->modelQuery(Level::class, $request, $this->search_attributes);
        return $query;
    }
}
