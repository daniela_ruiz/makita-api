<?php

namespace App\Providers;

use App\CreditNote;
use App\CustomerBenefit;
use App\Policies\CreditNotePolicy;
use App\Policies\CustomerBenefitPolicy;
use App\Policies\PurchasePolicy;
use App\Policies\WorkshopPolicy;
use App\Purchase;
use App\Workshop;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Purchase::class => PurchasePolicy::class,
        CustomerBenefit::class => CustomerBenefitPolicy::class,
        CreditNote::class => CreditNotePolicy::class,
        Workshop::class => WorkshopPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::tokensExpireIn(now()->addYear());

        Passport::refreshTokensExpireIn(now()->addYear(5));
    }
}
