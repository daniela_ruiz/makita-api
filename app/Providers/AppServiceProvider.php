<?php

namespace App\Providers;

use App\CustomerBenefit;
use App\Facades\Classes\DateString;
use App\Facades\Classes\FirebaseNotification;
use App\Observers\CustomerBenefitObserver;
use App\Observers\PurchaseObserver;
use App\Purchase;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // For error Specified key was too long error
        Schema::defaultStringLength(191);
        Purchase::observe(PurchaseObserver::class);
        CustomerBenefit::observe(CustomerBenefitObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton("FirebaseNotification", function () {
            return new FirebaseNotification();
        });

        $this->app->singleton("DateString", function () {
            return new DateString();
        });
    }
}
