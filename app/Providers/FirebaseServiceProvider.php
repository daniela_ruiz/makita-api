<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Kreait\Firebase;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Factory as FirebaseFactory;

class FirebaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Firebase::class, function () {
            return (new FirebaseFactory())
                ->withServiceAccount(ServiceAccount::fromJsonFile(base_path('google-service-account.json')))
                ->create();
        });

        $this->app->alias(Firebase::class, 'firebase');
    }
}
