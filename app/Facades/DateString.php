<?php
/**
 * Created by PhpStorm.
 * User: apokdev
 * Date: 10/23/18
 * Time: 5:32 AM
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class DateString extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "DateString";
    }
}
