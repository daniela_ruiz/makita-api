<?php
/**
 * Created by PhpStorm.
 * User: apokdev
 * Date: 10/19/18
 * Time: 11:20 AM
 */

namespace App\Facades\Classes;

class FirebaseNotification
{
    public function sendNotification($ids, $data)
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://fcm.googleapis.com'
        ]);
        $client->post('fcm/send', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key='. env('FIREBASE_MESSAGING_SERVER_KEY'),
            ],
            'json' => [
                'registration_ids' => $ids,
                'data' => $data
            ]
        ]);
    }
}
