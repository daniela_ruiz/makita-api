<?php
/**
 * Created by PhpStorm.
 * User: apokdev
 * Date: 10/19/18
 * Time: 11:20 AM
 */

namespace App\Facades\Classes;

class DateString
{
    public function string($val, $time)
    {
        switch ($time) {
            case 'A':
                $string = 'years';
                break;
            case 'M':
                $string = 'months';
                break;
            case 'D':
                $string = 'days';
                break;
            default:
                $string = 'months';
        }
        return "$val $string";
    }
}
