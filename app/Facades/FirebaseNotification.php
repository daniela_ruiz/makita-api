<?php
/**
 * Created by PhpStorm.
 * User: apokdev
 * Date: 10/19/18
 * Time: 11:38 AM
 */
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FirebaseNotification extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "FirebaseNotification";
    }
}