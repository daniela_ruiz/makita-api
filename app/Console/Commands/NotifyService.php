<?php

namespace App\Console\Commands;

use App\Configuration;
use App\Facades\FirebaseNotification;
use App\Purchase;
use Illuminate\Console\Command;

class NotifyService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:service';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia una notificacion a la app cuando se acerque el mantenimiento del producto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $purchases = Purchase::whereRaw(
            '(DATEDIFF(next_service, CURDATE())) <= ?',
            [Configuration::get('notification_interval', 5)]
        )->get();
        foreach ($purchases as $purchase) {
            if ($purchase->customer) {
                if ($purchase->customer->devices) {
                    $tokens = [];
                    $message = [
                        "id" => $purchase->id,
                        "type" => "productos",
                        "title" => "Se acerca la fecha de mantenimiento de tu producto",
                        "message" => "Tu producto: {$purchase->product->name} tiene fecha de mantenimiento: {$purchase->next_service} acércate a nuestros talleres para realizar el servicio",
                    ];
                    foreach ($purchase->customer->devices as $device) {
                        $tokens[] = $device->device_id;
                    }
                    FirebaseNotification::sendNotification($tokens, $message);
                }
            }
        }
        $this->info('Los mensajes han sido enviados correctamente');
    }
}
