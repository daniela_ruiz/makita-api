<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerBenefit extends Model
{
    use ModelQueryTrait;
    protected $table = "benefit_customer";

    protected $with = ["workshop"];

    protected $casts = [
        'approved' => 'boolean',
        'price' => 'float'
    ];

    protected $attributes = [
        'approved' => false
    ];

    protected $search_attributes = [
        'approved'
    ];

    protected $hidden = ["deleted_at"];

    public function workshop()
    {
        return $this->belongsTo('App\Workshop');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function benefit()
    {
        return $this->belongsTo('App\Benefit');
    }

    public function purchase(){
      return $this->belongsTo('App\Purchase');
    }

    public function scopeByCustomer($query, $filter)
    {
        $user = new User();
        $attributes = $user->search_attributes;
        $query->orWhereHas('customer.user', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    $query->orWhere($attribute, 'like', "%$filter%");
                }
            });
        });
        return $query;
    }

    public function scopeByWorkshop($query, $filter)
    {
        $workshop = new Workshop();
        $attributes = $workshop->search_attributes;
        $query->orWhereHas('workshop', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    $query->orWhere($attribute, 'like', "%$filter%");
                }
            });
        });
        return $query;
    }

    public function scopeByBenefit($query, $filter)
    {
        $benefit = new Benefit();
        $attributes = $benefit->search_attributes;
        $query->orWhereHas('benefit', function ($q) use ($filter, $attributes) {
            $q->where(function ($query) use ($filter, $attributes) {
                foreach ($attributes as $attribute) {
                    $query->orWhere($attribute, 'like', "%$filter%");
                }
            });
        });
        return $query;
    }
    public function scopeByApproved($query, $bool)
    {
        if ($bool == 'true' || $bool == 'false') {
            $val = $bool == 'true' ? true : false;
            $query->where('approved', $val);
        }
        return $query;
    }

    public function queryCustomerBenefits(Request $request)
    {
//        $query = $this->modelQuery(CustomerBenefit::class, $request, $this->search_attributes);
//        if ($request->has('filter')) {
//            $filter = $request->get('filter');
//            $query->byCustomer($filter);
//            $query->byWorkshop($filter);
//            $query->byBenefit($filter);
//        }
//        if ($request->has('approved')) {
//            $approved = $request->get('approved');
//            $query->byApproved($approved);
//        }
//        return $query;


        $filter = $request->query('filter', null);
        $from = $request->input('from', null);
        $to = $request->input('to', null);
        $orderBy = $this->getSortQuery($request);
        if ($orderBy[0] == 'status') {
            $orderBy[0] = 'approved';
        }

        $attributes = $this->search_attributes;

        $query = CustomerBenefit::where(
            function ($query) use ($filter, $attributes) {
                if ($filter) {
                    $query->where(function ($query) use ($filter, $attributes) {
                        foreach ($attributes as $attribute) {
                            $query->orWhere($attribute, 'like', "%$filter%");
                        }
                    });
                    $query->byCustomer($filter);
                    $query->byWorkshop($filter);
                    $query->byBenefit($filter);
                }
            }
        );
        if (!empty($from) && !empty($to)) {
            $dateFrom = Carbon::createFromFormat('Y-m-d', $from)->hour(00)->minute(00)->second(00);
            $dateTo = Carbon::createFromFormat('Y-m-d', $to)->hour(23)->minute(59)->second(59);

            $query->whereBetween('benefit_customer.created_at', [$dateFrom, $dateTo]);
        }

        if ($request->has('customer')) {
            $query->where('customer_id', $request->input('customer'));
        }
        if ($request->has('benefit')) {
          $query->where('benefit_id', $request->input('benefit'));
        }
        if ($request->has('product')) {
          $query->join('purchases', 'purchases.id', '=', 'benefit_customer.purchase_id')
            ->where('purchases.product_id',$request->input('product'));
        }

        $query->join(DB::raw("(SELECT users.name as 'username', customers.id as 'id' FROM customers JOIN users ON customers.user_id = users.id where customers.deleted_at is null) customer"), 'benefit_customer.customer_id', '=', 'customer.id')
            ->join('benefits', 'benefit_customer.benefit_id', '=', 'benefits.id')
            ->join('workshops', 'benefit_customer.workshop_id', '=', 'workshops.id')
            ->select('benefit_customer.*', 'customer.username', 'benefits.name as benefit_name', 'workshops.title as workshop_name')
            ->orderBy($orderBy[0], $orderBy[1]);

        return $query;
    }
    public static function getMostClaimedBenefits()
    {
        $benefits =  CustomerBenefit::with('benefit')->
        select('benefit_id', DB::raw('count(*) as total'))
            ->groupBy('benefit_id')
            ->orderBy('total', 'DESC')
            ->limit(5)
            ->get();
        return $benefits;
    }

    public static function getMostVisitedWorkshops()
    {
        $workshops = CustomerBenefit::with('workshop')->
        select('workshop_id', DB::raw('count(*) as total'))
            ->groupBy('workshop_id')
            ->orderBy('total', 'DESC')
            ->limit(5)
            ->get();
        return $workshops;
    }
}
