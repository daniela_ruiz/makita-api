<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Segment extends Model
{
    use ModelQueryTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public $search_attributes = ['name'];

    public function querySegments(Request $request)
    {
        if (!$request->has('sort')) {
            $request->request->add(['sort' => 'name|ASC']);
        }
        $query = $this->modelQuery(Segment::class, $request, $this->search_attributes);

        return $query;
    }
}
