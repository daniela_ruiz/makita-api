<?php

namespace App;

use App\Traits\Enums;
use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Notification extends Model
{
    use ModelQueryTrait, Enums;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'type',
        'date'
    ];

    public $search_attributes = [
        'title',
        'description',
        'type',
        'date'
    ];

    protected $enumTypes = [
        'd' => 'Descuentos',
        'p' => 'Promociones',
        'e' => 'Eventos',
    ];

    public function getImageAttribute($value)
    {
        return !is_null($value) ? env('APP_ENV') == 'production' ? secure_url(Storage::url($value)) : url(Storage::url($value)) : null;
    }

    public function queryNotifications(Request $request)
    {
        $query = $this->modelQuery(Notification::class, $request, $this->search_attributes);

        return $query;
    }
}
