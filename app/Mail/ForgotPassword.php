<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $password;
    public $email;

    /**
     * Create a new message instance.
     *
     * @param \App\Notification $notification
     * @param $customerId
     */
    public function __construct($newPassword, $email)
    {
        $this->password = $newPassword;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            return $this
                ->subject('Restablecer contraseña de Makita')
                ->markdown('emails.reset_password')
                ->with(['password' => $this->password, 'email' => $this->email]);
        } catch (\Exception $e) {
            return null;
        }
    }
}
