<?php

namespace App\Mail;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExtendedWarrantyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $purchase;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf = PDF::loadView('emails.warranty_mail', ['purchase' => $this->purchase]);
        try {
            return $this
                ->subject('Garantía Extendida')
                ->text("emails.warranty_text")
                ->attachData($pdf->output(), 'garantia.pdf');
        } catch (\Exception $e) {
            return null;
        }
    }
}
