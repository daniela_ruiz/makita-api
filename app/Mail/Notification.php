<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Notification extends Mailable
{
    use Queueable, SerializesModels;

    public $notification;
    public $customerId;

    /**
     * Create a new message instance.
     *
     * @param \App\Notification $notification
     * @param $customerId
     */
    public function __construct(\App\Notification $notification, $customerId)
    {
        $this->notification = $notification;
        $this->customerId = $customerId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            return $this
                ->subject($this->notification->title)
                ->markdown('emails.notifications')
                ->with(['notification' => $this->notification, 'unsubscribe' => $this->customerId]);
        } catch (\Exception $e) {
            return null;
        }
    }
}
