<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class ReportModel extends Model {

  abstract static function getReportQuery(Request $request, $defaultSelect = [], $defaultGroupBy = []);

  static protected function processDatesInQuery($query, Request $request, $date_field = "created_at") {
    if ($request->has("from") && $request->has("to")) {
      $from = Carbon::createFromFormat("Y-m-d", $request->input("from"))
        ->hour(0)->minute(0)->second(0);
      $to = Carbon::createFromFormat("Y-m-d", $request->input("to"))
        ->hour(23)->minute(59)->second(59);
      $query->whereBetween($date_field, [$from, $to]);
    }
  }
}
