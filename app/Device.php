<?php

namespace App;

use App\Traits\ModelQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Device extends Model
{
    use ModelQueryTrait;

    public $table = 'devices';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'customer_id',
        'device_id',
        'active'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'device_id';
    }

    public function customer()
    {
        return $this->belongsTo("App\Customer");
    }

    public $search_attributes = ['token'];

    public function queryDevices(Request $request)
    {
        $query = $this->modelQuery(Device::class, $request, $this->search_attributes);

        return $query;
    }
}
