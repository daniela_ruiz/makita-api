<?php

namespace App\Events;

use App\CustomerBenefit;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BenefitApprovedEvent {
    use Dispatchable, InteractsWithSockets, SerializesModels;
    var $customerBenefit;
    var $price;

  /**
   * Create a new event instance.
   *
   * @param CustomerBenefit $customerBenefit
   * @param                 $price
   */
    public function __construct(CustomerBenefit $customerBenefit, $price) {
        $this->customerBenefit = $customerBenefit;
        $this->price = $price;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return new PrivateChannel('channel-name');
    }
}
