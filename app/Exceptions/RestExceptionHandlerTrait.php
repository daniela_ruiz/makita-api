<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

trait RestExceptionHandlerTrait
{

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Request $request, Exception $e)
    {
        switch (true) {
            case $this->isModelNotFoundException($e):
                $retval = $this->modelNotFound();
                break;
            case $this->isAuthorizationException($e):
                $retval = $this->authorization($e->getMessage());
                break;
            case $this->isAuthenticationException($e):
                $retval = $this->authentication($e->getMessage());
                break;
            case $this->isNotFoundException($e):
                $retval = $this->badRequest('Endpoint Not Found', 404);
                break;
            case $this->isUnprocessableEntityException($e):
                $retval = $this->unprocessableEntity($e->getMessage());
                break;
            default:
                $retval = $this->badRequest($e->getMessage());
        }
        return $retval;
    }

    /**
     * Returns json response for generic bad request.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function badRequest($message = 'Bad Request', $statusCode = Response::HTTP_BAD_REQUEST)
    {
        return $this->jsonResponse([
            'code' => $statusCode,
            'message' => $message
        ], $statusCode);
    }

    /**
     * Returns json response for generic bad request.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function unprocessableEntity(
        $message = 'Unprocessable Entity',
        $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY
    ) {
        return $this->jsonResponse([
            'code' => $statusCode,
            'message' => $message
        ], $statusCode);
    }

    /**
     * Returns json response for Eloquent model not found exception.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function modelNotFound($message = 'Record Not Found', $statusCode = Response::HTTP_NOT_FOUND)
    {
        return $this->jsonResponse([
            'code' => $statusCode,
            'message' => $message
        ], $statusCode);
    }

    /**
     * Returns json response.
     *
     * @param array|null $payload
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse(array $payload = null, $statusCode = Response::HTTP_NOT_FOUND)
    {
        $payload = $payload ?: [];

        return response()->json($payload, $statusCode);
    }

    /**
     * Returns json response for Authorization exception.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function authorization($message = 'Forbidden', $statusCode = Response::HTTP_FORBIDDEN)
    {
        return $this->jsonResponse([
            'code' => $statusCode,
            'message' => $message
        ], $statusCode);
    }

    /**
     * Returns json response for Authentication exception.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function authentication($message = 'Unauthorized', $statusCode = Response::HTTP_UNAUTHORIZED)
    {
        return $this->jsonResponse([
            'code' => $statusCode,
            'message' => $message
        ], $statusCode);
    }

    /**
     * Determines if the given exception is an Eloquent model not found.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isModelNotFoundException(Exception $e)
    {
        return $e instanceof ModelNotFoundException;
    }

    /**
     * Determines if the given exception is an Authorization Exception.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isAuthorizationException(Exception $e)
    {
        return $e instanceof AuthorizationException;
    }

    /**
     * Determines if the given exception is an Authentication Exception.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isAuthenticationException(Exception $e)
    {
        return $e instanceof AuthenticationException;
    }

    protected function isUnprocessableEntityException(Exception $e)
    {
        return $e instanceof UnprocessableEntityHttpException;
    }

    /**
     * Determines if the given exception is an HTTP Not Found Exception.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isNotFoundException(Exception $e)
    {
        return $e instanceof NotFoundHttpException;
    }
}