<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Authentication
 **/
//Register
Route::post('users/register', 'Auth\RegisterController@create');
//Login
Route::post('auth/login', 'Auth\LoginController@login');
//Firebase
Route::post('auth/login/firebase', 'Auth\FirebaseLoginController@login');
//Login refresh token
Route::post('auth/refresh', 'Auth\LoginController@refreshToken');
//Logout
Route::get('auth/logout', 'Auth\LogoutController@logout')->middleware('auth:api');

//Forgot Password
Route::post('auth/forgot_password', 'Auth\ForgotPasswordController@sendResetLinkEmail');

/**
 * Authentication
 **/

Route::get('stores', 'API\StoreController@index');
Route::get('stores/{store}', 'API\StoreController@show');
Route::get('workshops', 'API\WorkshopController@index');
Route::get('workshops/{workshop}', 'API\WorkshopController@show');
Route::get('locations', 'API\StoreController@locations');
Route::get('video', 'API\UserController@showVideo');
Route::delete('devices/{device}', 'API\DeviceController@destroy');

Route::middleware('auth:api')->group(function () {
    Route::get('users/me', 'API\UserController@profile');
    Route::put('users/me', 'API\UserController@updateProfile');
    Route::put('users/change-password', 'API\UserController@changePassword');

    Route::middleware('role:admin,operator,accountant')->group(function(){
      Route::get('credit_notes', 'API\CreditNoteController@index')->middleware("can:viewAll,App\CreditNote");
      Route::get('credit_notes/{creditNote}', 'API\CreditNoteController@show')->middleware("can:view,creditNote");
      Route::put('credit_notes/{creditNote}/cancel', 'API\CreditNoteController@cancel')->middleware("can:cancel,creditNote");
      Route::put('credit_notes/{creditNote}/process', 'API\CreditNoteController@process')->middleware("can:process,creditNote");
      Route::delete('credit_notes/{creditNote}', 'API\CreditNoteController@destroy')->middleware("can:delete,creditNote");
    });

    Route::middleware('role:admin,operator,accountant')->group(function() {
        Route::prefix('report')->group(function(){
          Route::get('purchases', 'API\ReportController@purchases');
          Route::get('purchases/csv', 'API\ReportController@purchasesExport');

          Route::get('customer_benefit/product', 'API\ReportController@customerBenefitByProduct');
          Route::get('customer_benefit/product/csv', 'API\ReportController@customerBenefitByProductExport');

          Route::get('customer_benefit/benefit', 'API\ReportController@customerBenefitByBenefit');
          Route::get('customer_benefit/benefit/csv', 'API\ReportController@customerBenefitByBenefitExport');

          Route::get('credit_notes', 'API\ReportController@creditNotes');
          Route::get('credit_notes/csv', 'API\ReportController@creditNotesExport');

        });
    });

    Route::middleware('role:admin,operator')->group(function () {
        Route::resource('users', 'API\UserController')->only(['index', 'store', 'update', 'show']);

        Route::resource('customers', 'API\CustomerController')->except(['create', 'edit']);

        Route::get('customers/{customer}/purchases', 'API\PurchaseController@showUserPurchases');

        Route::post('customers', 'Auth\RegisterController@create');

        Route::get('customers/{customer}/benefits', 'API\BenefitController@claimedBenefits');

        Route::put('customer_benefit/{customerBenefit}/approve', 'API\BenefitController@approveBenefit');

        Route::delete('customer_benefit/{customerBenefit}', 'API\BenefitController@deleteClaimed');

        // Levels
        Route::resource('levels', 'API\LevelController')->only(['index', 'show']);

        // Roles
        Route::resource('roles', 'API\RoleController')->only(['index', 'show']);

        Route::post('workshops/createUser', 'API\WorkshopController@createUser')->middleware("can:addUser,App\Workshop");
        Route::post('workshops/addUser/{user}', 'API\WorkshopController@addUser')->middleware("can:addUser,App\Workshop");
    });

    Route::middleware('role:admin')->group(function () {
        // Users
        Route::resource('users', 'API\UserController')->except(['create', 'edit', 'index', 'store', 'update']);
        // Products
        Route::resource('products', 'API\ProductController')->except([
            'create', 'edit', 'index', 'show', 'update'
        ]);
        // Update products
        Route::post('products/{product}', 'API\ProductController@update');

        // Add Image products

        Route::post('products/{product}/image', 'API\ProductController@addImage');

        Route::delete('products/image/{productImage}', 'API\ProductController@deleteImage');

        // Categories
        Route::resource('categories', 'API\CategoryController')->except([
            'create', 'edit', 'index', 'show'
        ]);
        // Segments
        Route::resource('segments', 'API\SegmentController')->except([
            'create', 'edit', 'index', 'show'
        ]);
        // Roles
        Route::resource('roles', 'API\RoleController')->except([
            'create', 'edit', 'index', 'show'
        ]);
        // Benefits
        Route::resource('benefits', 'API\BenefitController')->except([
            'create', 'edit', 'index', 'show'
        ]);

        // Invoices
        Route::resource('invoices', 'API\InvoiceController')->except([
            'create', 'edit'
        ]);
        // Levels
        Route::resource('levels', 'API\LevelController')->except([
            'create', 'edit', 'update', 'index', 'show'
        ]);
        // Update levels
        Route::post('levels/{level}', 'API\LevelController@update');

        // Stores
        Route::resource('stores', 'API\StoreController')->except([
            'edit', 'create', 'index', 'show'
        ]);

        //Workshops
        Route::resource('workshops', 'API\WorkshopController')->except([
            'edit', 'create', 'index', 'show'
        ]);

        // Settings
        Route::resource('configurations', 'API\ConfigurationController')->only([
            'update','index', 'show'
        ]);

        //Invoices
        Route::apiResource('invoice', 'API\InvoiceController');
    });

    //Purchase
    Route::prefix('purchases')->group(function () {
        Route::get('valid', 'API\PurchaseController@myValidPurchases')->middleware('can:viewAll,App\Purchase');
      // Create purchase
        Route::post('', 'API\PurchaseController@store')->middleware('roles:customer');
        // Show Purchase
        Route::get('{purchase}', 'API\PurchaseController@show')->middleware('can:view,purchase');
        Route::get('', 'API\PurchaseController@index')->middleware('can:viewAll,App\Purchase');
        Route::post('{purchase}', 'API\PurchaseController@update')->middleware('can:update,purchase');
        Route::delete('{purchase}', 'API\PurchaseController@destroy')->middleware('role:admin');
        Route::put('{purchase}/verify', 'API\PurchaseController@updateStatus')->middleware('role:admin,operator');
        Route::put('{purchase}/service', 'API\PurchaseController@updateNextService')->middleware('role:admin,operator');
    });

    //Notifications
    Route::prefix('notifications')->group(function () {
        Route::post('', 'API\NotificationController@store')->middleware('role:admin');
        Route::get('{notification}', 'API\NotificationController@show');
        Route::get('', 'API\NotificationController@index');
        Route::post('{notification}', 'API\NotificationController@update')->middleware('role:admin');
        Route::delete('{notification}', 'API\NotificationController@destroy')->middleware('role:admin');
        Route::get('{notification}/send', 'API\NotificationController@sendNotification')
            ->middleware('role:admin');
        Route::get('subscribe', 'API\NotificationController@subscribe')->middleware('roles:customer');
    });

    Route::resource('categories', 'API\CategoryController')->only(['index', 'show']);
    Route::resource('segments', 'API\SegmentController')->only(['index', 'show']);
    Route::resource('products', 'API\ProductController')->only(['index', 'show']);
    Route::resource('benefits', 'API\BenefitController')->only(['index', 'show']);

    Route::post('benefits/claim', 'API\BenefitController@claim')->middleware('roles:customer');

    Route::get('benefits/claimed', 'API\BenefitController@claimedBenefits');
    Route::get('benefits/claimed/{customerBenefit}', 'API\BenefitController@claimedBenefit')
        ->middleware('roles:customer')->middleware('can:view,customerBenefit');

    //Firebase devices

    Route::post('devices', 'API\DeviceController@store')->middleware('roles:customer');

    Route::get('devices/{device}', 'API\DeviceController@show')->middleware('roles:customer');

    //Mails
    Route::get('mails/subscribe', 'API\NotificationController@subscribe')->middleware('roles:customer');
    Route::get('purchases/{purchase}/warranty', 'API\PurchaseController@sendWarranty')->middleware('roles:customer');

    //Dashboard
    Route::get('dashboard', 'API\AdminController@getDashboard');

    //Catalog
    Route::prefix('catalogues')->group(function () {
        Route::post('', 'API\CatalogueController@store')->middleware('role:admin');
        Route::get('{catalogue}', 'API\CatalogueController@show');
        Route::get('', 'API\CatalogueController@index');
        Route::post('{catalogue}', 'API\CatalogueController@update')->middleware('role:admin');
        Route::delete('{catalogue}', 'API\CatalogueController@destroy')->middleware('role:admin');
    });

});
